<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gift extends CI_Controller {

	/**
	 * Home Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct(){
    parent::__construct();
    $username= $this->session->userdata("username");
    $userType=$this->session->userdata('usertype');
    if (empty($username)) {
      $this->session->set_flashdata("error_login", "Invalid Request");
      redirect("login", "refresh");
    }
    $this->load->model('m_default');
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT b.package_name, b.price FROM tbl_member a,tbl_package b WHERE a.id=$member_id and a.package_id=b.id";
    $this->data['package_data'] = $this->m_default->get_single_row($sql);
    $sql="SELECT a.*,b.username, b.email FROM tbl_member a,tbl_login b WHERE a.id=$member_id and a.sponser_id=b.member_id";
    $this->data['sponser_data'] = $this->m_default->get_single_row($sql);
    
    $sql="select coin_price from tbl_coinprice where id=1";
    $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
    foreach($this->input->post() as $items){
      if ($items != ''){
          if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
            $this->session->set_flashdata('error_login','Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
            break;
          }
      }
    }
  }


  public function index(){
    $member_id=$this->session->userdata('member_id');
    $sql="select amount as wallet from tbl_wallet where member_id=$member_id";
    $this->data['wallet'] = $this->m_default->get_single_row($sql);

    $sql="select amount as fundwallet from tbl_fundwallet where member_id=$member_id";
    $this->data['fundwallet'] = $this->m_default->get_single_row($sql);
    $this->data['content']='gift/buy_giftcard';
    $this->data['title']='Buy Gift Card | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function getpackage(){
    $package=  $this->input->post('package');
    echo  $this->db->query("Select price from tbl_package where id=$package")->row()->price;
  }

  public function getmembername(){
    $member_id=  $this->input->post('member_id');
    echo  $this->db->query("Select name from tbl_member where id=$member_id")->row()->name;
  }

  public function getwallet(){
    $member_id=$this->session->userdata('member_id');
    $wallet=  $this->input->post('wallet');
    if($wallet==2){
      $wallet='Cash Wallet';
    }

    if($wallet==3){
      $wallet='I Wallet';
    }

    echo  $this->db->query("Select amount from tbl_wallet where member_id=$member_id and wallet_type='$wallet'")->row()->amount;
  }

  public function buy(){
    $member_id=$this->session->userdata('member_id');
    $package=$this->input->post('package');
    $gift_value=$this->input->post('gift_value');

    $pin=$this->input->post('pin');
    $remarks=$this->input->post('remarks');

    $wallet_balance=$this->input->post('wallet_balance');
    $goldift_value=$gift_value;
    $gift_value=$gift_value*$pin;

    $bonustype='Bought Gift Card';
    $flag=0;
    if($gift_value<=$wallet_balance){
      $query="update tbl_wallet set amount=amount-$gift_value where member_id=$member_id ";
      $this->m_default->execute_query($query);
      $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;
      $array=array('member_id'=>$member_id,'desc'=>$bonustype,'debited'=>$gift_value,'wallet_type'=>"Cash Wallet",'balance'=>$debitedamount);
      $this->m_default->data_insert('tbl_wallet_report',$array);
      $flag=1;
    }

    if($flag==1){
      for($i=1;$i<=$pin;$i++){
        while(1){
          $pingift=$this->random_password(8);
          $count=$this->db->query("Select count(id)  as count from tbl_giftcard where pin='$pingift'")->row()->count;
          if($count==0){
            $logindata = array('package' => $package,'amount'=>$goldift_value, 'pin'=>$pingift,'payment_type'=>$wallet,'created_by'=>$member_id,'remarks'=>$remarks);
            $this->m_default->data_insert('tbl_giftcard',$logindata);
            $this->session->set_flashdata('success' , 'Gift Card Added Successfully..!');
            break;
          }
        }
      }
      redirect('gift/list_giftcards');
    }
    else{
      $this->session->set_flashdata('danger' , 'Insufficient Balance..!');
      redirect('gift/list_giftcards');
    }
  }

  public function random_password( $length = 8 ){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
  }

  public function list_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE created_by=$member_id ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    $this->data['content']='gift/list_giftcards';
    $this->data['title']='Gift Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function used_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE created_by=$member_id and used=1 ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();

    foreach ($this->data['giftcards'] as $key => $value) {
      $this->data['giftcards'][$key]['sent_name']=$this->db->query('select name from tbl_member where id='. $this->data['giftcards'][$key]['created_by'])->row()->name;
      if($this->data['giftcards'][$key]['sent_to']!='' && $this->data['giftcards'][$key]['sent_to']!=null){
        $this->data['giftcards'][$key]['sent_to']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['sent_to'])->row()->username;
      }

      if($this->data['giftcards'][$key]['used_by']!='' && $this->data['giftcards'][$key]['used_by']!=null){
        $this->data['giftcards'][$key]['used_by']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['used_by'])->row()->username;
      }
    }

    $this->data['content']='gift/used_giftcards';
    $this->data['title']='Used E-pin Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function used_giftcards_admin(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE  used=1 ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    foreach ($this->data['giftcards'] as $key => $value) {
      $this->data['giftcards'][$key]['sent_name']=$this->db->query('select name from tbl_member where id='. $this->data['giftcards'][$key]['created_by'])->row()->name;
      if($this->data['giftcards'][$key]['sent_to']!='' && $this->data['giftcards'][$key]['sent_to']!=null){
        $this->data['giftcards'][$key]['sent_to']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['sent_to'])->row()->username;
      }
      if($this->data['giftcards'][$key]['used_by']!='' && $this->data['giftcards'][$key]['used_by']!=null){
        $this->data['giftcards'][$key]['used_by']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['used_by'])->row()->username;
      }
    }
    $this->data['content']='gift/used_giftcards';
    $this->data['title']='Used E-pin Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function fresh_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE used=0 ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    $this->data['content']='gift/unused_giftcards';
    $this->data['title']='Unused E-pin | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function unused_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE created_by=$member_id and used=0 ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    $this->data['content']='gift/unused_giftcards';
    $this->data['title']='Unused E-pin | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function send_giftcard(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE created_by=$member_id  and used=0 and sent_to=0";
    $this->data['giftcards']=$this->db->query($sql)->result_array();

    $sql="SELECT  member_id,username FROM tbl_login WHERE member_id!=$member_id ";
    $this->data['members']=$this->db->query($sql)->result_array();
    $this->data['content']='gift/send_giftcards';
    $this->data['title']='Send Gift Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function sent_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard WHERE created_by=$member_id and sent_to!=0 ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();

    foreach ($this->data['giftcards'] as $key => $value) {
      $this->data['giftcards'][$key]['sent_name']=$this->db->query('select name from tbl_member where id='. $this->data['giftcards'][$key]['sent_to'])->row()->name;
      $this->data['giftcards'][$key]['sent_to']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['sent_to'])->row()->username;
   }

    $this->data['content']='gift/sent_giftcards';
    $this->data['title']='Sent Gift Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function received_giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT * FROM tbl_giftcard WHERE sent_to=$member_id ";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    foreach ($this->data['giftcards'] as $key => $value) {
      $this->data['giftcards'][$key]['sent_name']=$this->db->query('select name from tbl_member where id='. $this->data['giftcards'][$key]['created_by'])->row()->name;
      $this->data['giftcards'][$key]['sent_to']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['created_by'])->row()->username;
    }
    $this->data['content']='gift/received_giftcards';
    $this->data['title']='Received Gift Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function count_gift(){
    $member_id=$this->session->userdata('member_id');
    $package=  $this->input->post('package');
    echo  $this->db->query("Select count(id) as count from tbl_giftcard where created_by=$member_id and package=$package and sent_to=0 and used=0")->row()->count;
  }

  public function send(){
    $member_id=$this->session->userdata('member_id');
    $package=  $this->input->post('package');
    $gift_available=  $this->input->post('gift_available');
    $epin=  $this->input->post('epin');
    $sent_to=  $this->input->post('member_id');

    $count=  $this->db->query("Select count(id) as count from tbl_giftcard where created_by=$member_id and package=$package and sent_to=0 and used=0 ")->row()->count;
    if($count>=$epin){
      for($i=1;$i<=$epin;$i++){
        $this->db->query("update tbl_giftcard set sent_to =$sent_to where created_by=$member_id and package=$package and sent_to=0 and used=0 ORDER BY ID
        DESC LIMIT 1;" );
      }
      $this->session->set_flashdata('success' , 'Gift Card Successfully Sent!');
      redirect('gift/sent_giftcards');
    }
    else{
      $this->session->set_flashdata('danger' , 'Gift Card Not Available..!');
      redirect('gift/send_giftcard');
    }
  }

  public function giftcards(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  * FROM tbl_giftcard";
    $this->data['giftcards']=$this->db->query($sql)->result_array();
    foreach ($this->data['giftcards'] as $key => $value) {
      $this->data['giftcards'][$key]['sent_name']=$this->db->query('select name from tbl_member where id='. $this->data['giftcards'][$key]['created_by'])->row()->name;
      $this->data['giftcards'][$key]['sent_to']=$this->db->query('select username from tbl_login where member_id='. $this->data['giftcards'][$key]['created_by'])->row()->username;
    }

    $this->data['content']='gift/giftcards';
    $this->data['title']='List Gift Cards | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function add_giftcard(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  member_id,username FROM tbl_login WHERE member_id!=$member_id ";
    $this->data['members']=$this->db->query($sql)->result_array();
    $this->data['content']='gift/add_giftcard';
    $this->data['title']='Add Gift Card | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function admin_buy(){
    $member_id=$this->input->post('member_id');
    $package=$this->input->post('package');
    $gift_value=$this->input->post('gift_value');
    $pin=$this->input->post('pin');
    $goldift_value=$gift_value;
    $remarks="Created by Admin";
    for($i=1;$i<=$pin;$i++){
      while(1){
        $pingift=$this->random_password(8);
        $count=$this->db->query("Select count(id)  as count from tbl_giftcard where pin='$pingift'")->row()->count;
        if($count==0){
          $logindata = array('package' => $package,'amount'=>$goldift_value, 'pin'=>$pingift,'payment_type'=>$wallet,'created_by'=>$member_id,'remarks'=>$remarks);
          $this->m_default->data_insert('tbl_giftcard',$logindata);
          break;
        }
      }
    }

    if($pin>=50){
      $this->db->query("update tbl_member set franchise=1 where id=".$member_id);
    }
    $this->session->set_flashdata('success' , 'Gift Card Added Successfully..!');
    redirect('gift/giftcards');
  }
}

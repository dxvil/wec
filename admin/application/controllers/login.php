<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  /**
   * Home Controller: For Loading home page
   * Author: Dipanwita
   */

  public function __construct()
  {
   parent::__construct();
   $this->load->model('m_default');
   $this->load->model('includes/mail');
   if ($this->input->post()){
    foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-_]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
    }
  }

  public function maintaince(){
    echo '<!doctype html>
    <title>Site Maintenance</title>
    <style>
      body { text-align: center; padding: 150px; }
      h1 { font-size: 50px; }
      body { font: 20px Helvetica, sans-serif; color: #333; }
      article { display: block; text-align: left; width: 650px; margin: 0 auto; }
      a { color: #dc8100; text-decoration: none; }
      a:hover { color: #333; text-decoration: none; }
    </style>
    
    <article>
        <h1>We&rsquo;ll be back soon!</h1>
        <div>
            <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always <a href="mailto:#">contact us</a>, otherwise we&rsquo;ll be back online shortly!</p>
            <p>&mdash; The Team</p>
        </div>
    </article>';
  }

  public function index()  //to load home page
  {
   $this->data['title']='Login | WAVE EDU COIN';
   $this->load->view('login',$this->data);
  }

  public function get_coin_price(){

    $coin_price = $this->db->query('select coin_price from tbl_coinprice where id = 1')->row()->coin_price;
    echo json_encode(array('current_price' => number_format($coin_price/70,  5)));

  }
  
  function callAPI($method, $url, $data){
    $curl = curl_init();
 
    switch ($method){
       case "POST":
          curl_setopt($curl, CURLOPT_POST, 1);
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          break;
       case "PUT":
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
          break;
       default:
          if ($data)
             $url = sprintf("%s?%s", $url, http_build_query($data));
    }
 
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
 
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
 }
  
  public function admin_otp()  //to load home page
  {
      
      $otp = $this->generateRandomString();
       
      $contact_no =$this->db->query('select contact_number from tbl_member where id=1')->row()->contact_number;
      
             /**msg**/
					
			$to = $contact_no;
      $message = 'To Login Admin Panel Please use the OTP. OTP: '.$otp;

      // $ID = 'AC18eca841c5aef9da19c48304bd1a053f';
      // $token = 'fb27e4bbd7f4709def644162b2daf91b';
      // $service = 'AB1234567890abcdef1234567890abcdef';
      // $url = 'https://api.twilio.com/2010-04-01/Accounts/' . $ID . '/Messages.json';

      // $ch = curl_init();
      // curl_setopt($ch, CURLOPT_URL,$url);
      // curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

      // curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
      // curl_setopt($ch, CURLOPT_USERPWD,$ID . ':' . $token);

      // curl_setopt($ch, CURLOPT_POST,true);
      // curl_setopt($ch, CURLOPT_POSTFIELDS,
      //     'To=' . rawurlencode('+' . $to) .
      //     '&From=' . rawurlencode('+15094361575') .
      //     '&Body=' . rawurlencode($message));

      // $resp = curl_exec($ch);
      // curl_close($ch);
      $l = "http://bulksms.mediastrings.com/http-api.php?username=suman123&password=123456&senderid=CCRYPT&route=1&number=$to&message=$message";
      $url = preg_replace("/ /", "%20", $l);
      file_get_contents($url);

      // $s = $this->callAPI('GET', $l, false);

      // print_r($s);

    
			// if($this->msg91->send($to, $message) == TRUE)  {
			    
			// }
			// else{
			   
      // }
      

			/**msg**/
			
			
			$email="aadhaarreport@gmail.com";
			
			
			$subject = 'OTP';
     
      $message = "
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset='utf-8'>
          <title></title>
        </head>
        <body>
        
        <p>To Login Admin Panel Please use the OTP.</p>
        <table>
        
        <tr>
          <td>OTP:</td><td>$otp</td>
        </tr>
        
        <tr>
          <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        
        <tr>
          <td></td><td></td>
        </tr>
        
      </table>
       
      <p>Team WaveEduCoin</p>
</body>
      </html>";

      $this->mail->sendEmail($email,$subject,$message);
			
			
			$logindata = array('otp' => $otp);
            $this->m_default->data_insert('tbl_admin_otp',$logindata);
			
  }
  
  public function check_otp()  //to load home page
  {
      
      $otp=$this->input->post('otp');
       
      $count =$this->db->query("select count(id) as count from tbl_admin_otp where  otp='$otp' and status=0")->row()->count;
      $this->db->query("update tbl_admin_otp set status=1 where  otp='$otp' and status=0");
      
      if($count==0){
          echo 1;
      }
      else {
          echo 0;
      }
	   
			
  }
  
  	

  public function signup()  //to load home page
  {
     $spid= $this->input->get('spid');
     $this->data['spname'] = $this->db->query('select name from tbl_member where id in(select member_id from tbl_login where username="'.$spid.'")')->row()->name;
   $this->data['title']='Signup | WAVE EDU COIN';
   $this->load->view('signup',$this->data);
  }

  // Login form server_site validation
  public function check_login($value='')
  {
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    if ($this->form_validation->run() == FALSE)
      {
        $this->load->view('login');
      }
      else
      {
          $this->valid_login();
      }
  }


  public function save_member()
  {

    // foreach($this->input->post() as $items){

    //   if (! preg_match("/^[a-z0-9A-Z@.-]+$/i", $items)){
    //     $this->session->set_flashdata('error_login','Something went wrong');
    //     redirect('login/signup');
    //     break;
    //   }

    // }

    // $this->form_validation->set_rules('email', 'Email', 'is_unique[tbl_login.email]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('sponser_id', 'Sponser ID', 'required');
    $this->form_validation->set_rules('direction', 'Direction', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('contact_no', 'Contact No', 'required');
    //$this->form_validation->set_rules('user_id', 'User Id', 'required|is_unique[tbl_login.username]');
//    $this->form_validation->set_rules('city', 'City', 'required');
//    $this->form_validation->set_rules('bitcoin_address', 'Adhar Number', 'required');

//     $this->form_validation->set_rules('bank_name', 'Bank name', 'required');
//    $this->form_validation->set_rules('ifsc_code', 'Ifsc code', 'required');
//    $this->form_validation->set_rules('account_holder_name', 'Account holder name', 'required');
//    $this->form_validation->set_rules('branch', 'Branch', 'required');
//    $this->form_validation->set_rules('account_number', 'Account number', 'required');
//    $this->form_validation->set_rules('address', 'Address', 'required');
//    $this->form_validation->set_rules('date', 'Date', 'required');
//    $this->form_validation->set_rules('plan', 'Plan', 'required');

    $plan=$this->input->post('plan');

    if ($this->form_validation->run() == FALSE)
      {
        $this->session->set_flashdata('error_login',validation_errors());
        redirect('login/signup');
      }
      else{
       $sponser_id=$this->input->post('sponser_id');

       $query= $query=$this->db->query("select a.member_id from tbl_login a,tbl_member b where a.username='$sponser_id' and b.id=a.member_id and b.member_status=1")->result_array();

       if(count($query)>0)
       {

                            $query1=$this->db->query("select id from tbl_login where email='".$email."'")->result_array();

 if(count($query1)==3)      {
$this->session->set_flashdata('error_login','Maximum 3 person allowed under one Email..!');
        redirect('login/signup');
}

                $direction=$this->input->post('direction');

                $parent_id=$this->recursive_leg($query[0]['member_id'],$direction);
                

               $sql="SELECT parent_id,leg FROM tbl_member  where id=".$query[0]['member_id'];
             $nextstep=$this->db->query($sql)->result_array();
               
if($nextstep[0]['parent_id']!=""){
                $this->recursive_leg_sponser($nextstep[0]['parent_id'],$nextstep[0]['leg']);
            }


        $name=$this->input->post('name');

        $contact_no=$this->input->post('contact_no');
        $email=$this->input->post('email');
        //$user_id=$this->input->post('user_id');

      //  $password=$this->rand_string(8);
        $password=$this->input->post('password');
        //$confirm_password=$this->input->post('confirm_password');
        $country=$this->input->post('country');
        $bitcoin_address=$this->input->post('bitcoin_address');
        $bank_name=$this->input->post('bank_name');
        $ifsc_code=$this->input->post('ifsc_code');
        $account_holder_name=$this->input->post('account_holder_name');
        $branch=$this->input->post('branch');
        $account_number=$this->input->post('account_number');
        $address=$this->input->post('address');
        $date=$this->input->post('date');
        $city=$this->input->post('city');
        $pan_no=$this->input->post('pan_no');
        $nominee_name=$this->input->post('nominee_name');
        $relationship=$this->input->post('relationship');
        $birth_date=$this->input->post('birth_date');
        $nominee_name=$this->input->post('nominee_name');
        $dob=$this->input->post('dob');
        $gender=$this->input->post('gender');
        $pin_code=$this->input->post('pin_code');

                if($plan==7)
                {
                    $pv=10;
                }
                else if($plan==8)
                {
                    $pv=20;
                }
                else{
                    $pv=1;
                }
                
                if($plan==1){
                    $package_price=1000;
                }
                else if($plan==2){
                    $package_price=2000;
                }
                 else if($plan==3){
                    $package_price=5000;
                }
                else if($plan==4){
                    $package_price=8000;
                }
                else if($plan==5){
                    $package_price=12000;
                }
                else if($plan==6){
                    $package_price=15000;
                }
                else if($plan==7){
                    $package_price=9999;
                }
                else if($plan==8){
                    $package_price=19999;
                }
                else{
                    $package_price=0;
                }
                
                
                
            $data = array('package_price'=>$package_price,'relationship'=>$relationship,'birth_date'=>$birth_date,'nominee_name'=>$nominee_name,'dob'=>$dob,'gender'=>$gender,'pin_code'=>$pin_code,'pan_no'=>$pan_no,'city'=>$city,'package_id'=>$plan,'name' => $name,'sponser_id'=>$query[0]['member_id'], 'leg'=> $direction,'country'=>$country,'parent_id'=>$parent_id,'user_image'=>'user.png','bitcoin_address'=>$bitcoin_address,'bank_name'=>$bank_name,'ifsc_code'=>$ifsc_code,'account_holder_name'=>$account_holder_name,
        'branch'=>$branch,'account_number'=>$account_number,'address'=>$address,'date'=>date('Y-m-d',strtotime($date)));
        $member_id=$this->m_default->data_insertwithid('tbl_member',$data);

                $shinerandy=$this->rand_string(3);
        $userid='WEC0'.$member_id.$shinerandy;

        $logindata = array('member_id' => $member_id,'email'=>$email, 'username'=>$userid,'password'=>md5($password),'usertype'=>2);
        $this->m_default->data_insert('tbl_login',$logindata);

        $array=array('user_id'=>$member_id,'amount'=>0.00);
        $this->m_default->data_insert('tbl_bonus_wallet',$array);

        $records=array();
        // $sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$query[0]['member_id'];
        // $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
        // $records=$this->db->query($sql)->result_array();

        // if(count($records)<2)
        // {
        //  $matrixparent_id= 2;
        // }
        // else{

        //  $matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
        // }

        // $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
        // $this->m_default->data_insert('tbl_matrix',$matrixdata);


        $walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
        $this->m_default->data_insert('tbl_wallet',$walletdata);
        
        
        $walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
        $this->m_default->data_insert('tbl_fundwallet',$walletdata);
        
        
      
        $this->m_default->data_insert('tbl_repurchase',$walletdata);
        
        $array=array('member_id'=>$member_id,'left_used'=>0,'right_used'=>0,'total_pairs'=>0,'rank'=>500);
                $this->m_default->data_insert('tbl_pairsdata',$array);
                
                 
        $array=array('member_id'=>$member_id,'coins'=>0);
        $this->m_default->data_insert('tbl_coin',$array);
        
        $array=array('member_id'=>$member_id,'coins'=>0);
        $this->m_default->data_insert('tbl_release_wallet',$array);
        

        // $teamdevelopdata = array('member_id' => $member_id,'left_ids'=>'','right_ids'=>'');
        // $this->m_default->data_insert('tbl_team_development',$teamdevelopdata);
        
        // for($rank=0;$rank<=9;$rank++){
        // $pairsdata = array('member_id' => $member_id,'left_used'=>0,'right_used'=>0,'rank'=>$rank);
        // $this->m_default->data_insert('tbl_pairsdata',$pairsdata);
        // }
        
        
        
       
        
        $sub="Registration";
				$msg='<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>email</title>
</head>
<body>

 
  <div style="width:100%; display:block; margin:0 auto;">
    <div style="">
      <div style="padding:30px ; background:#1e5597;">
        <div style="position:absolute ; top:0; left:0; right:0; bottom:0;background:#4142589c;"></div>
        <img src="https://www.waveeducoins.com/images/wec logo.png" style="width:70px;" alt="logo" style="position:relative; display:inline-block;">
        <h2 style="color:#fff;position:relative; display:inline-block;     margin-left: 255px;">Welcome to Wave Edu Coin</h2>
      </div>
      <div style="background:#ecebeb; padding:30px ;text-align: center;">
        <h1>Thank you for singing up with us</h1>
        <p>Your Username is "'.$userid.'" and Password is  "'.$password.'"</p>
       
        <a href="'.$link.'"> Click the link</a>
      </div>
      <div style="background:#1e5597; position: relative; padding:30px ;">
        <div style="position:absolute ; top:0; left:0; right:0; bottom:0;background:#4142589c;"></div>
        Wave Edu Coin
        <br>
        http://www.waveeducoins.com
       
      </div>
    </div>
  </div>
</body>
</html>
'; 

 
        $this->mail->sendEmail($email,$sub,$msg);

        $this->session->set_flashdata('success','Registered Successfully..! <br>Your Username is '.$userid. ' and Password is '.$password );
        redirect('login/signup');

       }
       else {
          $this->session->set_flashdata('error_login','Sponser Id Does not exist..!');
          redirect('login/signup');
      }
      redirect('login/signup');
      }
      redirect('login/signup');
  }
  
  
  
    public function save_member_demo()
  {
    for($i=0;$i<=20;$i++){

    $plan=2;

    
       $sponser_id='IW000300';

       $query= $query=$this->db->query("select a.member_id from tbl_login a,tbl_member b where a.username='$sponser_id' and b.id=a.member_id and b.member_status=1")->result_array();

       if(count($query)>0)
       {

                            /*$query=$this->db->query("select id from tbl_member where parent_id='".$query[0]['id']."'")->result_array();

 if(count($query)==2)      {
$this->session->set_flashdata('error_login','Maximum 2 person reaches under your Sponser Id');
        redirect('login/signup');
}
*/
                $direction=0;

                $parent_id=$this->recursive_leg($query[0]['member_id'],$direction);
                
                $this->recursive_leg_sponser($query[0]['member_id'],$direction);

        $name='Demo';

        $contact_no='000000000000';
        $email='demo@gmail.com';
        
        
        $country=$this->input->post('country');
        $bitcoin_address=$this->input->post('bitcoin_address');
                $bank_name=$this->input->post('bank_name');
        $ifsc_code=$this->input->post('ifsc_code');
        $account_holder_name=$this->input->post('account_holder_name');
        $branch=$this->input->post('branch');
        $account_number=$this->input->post('account_number');
        $address=$this->input->post('address');
        $date=$this->input->post('date');
                $city=$this->input->post('city');
                $pan_no=$this->input->post('pan_no');
                $nominee_name=$this->input->post('nominee_name');
                $relationship=$this->input->post('relationship');
                $birth_date=$this->input->post('birth_date');
                $nominee_name=$this->input->post('nominee_name');
                $dob=$this->input->post('dob');
                $gender=$this->input->post('gender');
                $pin_code=$this->input->post('pin_code');
        //$user_id=$this->input->post('user_id');

        $password=$this->rand_string(8);
        //$password=$this->input->post('password');
        //$confirm_password=$this->input->post('confirm_password');
        
               

                if($plan==7)
                {
                    $pv=10;
                }
                else if($plan==8)
                {
                    $pv=20;
                }
                else{
                    $pv=1;
                }
                
                if($plan==1){
                    $package_price=1000;
                }
                else if($plan==2){
                    $package_price=2000;
                }
                 else if($plan==3){
                    $package_price=5000;
                }
                else if($plan==4){
                    $package_price=8000;
                }
                else if($plan==5){
                    $package_price=12000;
                }
                else if($plan==6){
                    $package_price=15000;
                }
                else if($plan==7){
                    $package_price=9999;
                }
                else if($plan==8){
                    $package_price=19999;
                }
                else{
                    $package_price=0;
                }
                
                
                $relationship="wife";
            $data = array('member_status'=>1,'package_price'=>$package_price,'pv'=>$pv,'relationship'=>$relationship,'birth_date'=>$birth_date,'nominee_name'=>$nominee_name,'dob'=>$dob,'gender'=>$gender,'pin_code'=>$pin_code,'pan_no'=>$pan_no,'city'=>$city,'package_id'=>$plan,'name' => $name,'sponser_id'=>$query[0]['member_id'], 'leg'=> $direction,'contact_number'=>$contact_no,'country'=>$country,'parent_id'=>$parent_id,'user_image'=>'user.png','bitcoin_address'=>$bitcoin_address,'bank_name'=>$bank_name,'ifsc_code'=>$ifsc_code,'account_holder_name'=>$account_holder_name,
        'branch'=>$branch,'account_number'=>$account_number,'address'=>$address,'date'=>date('Y-m-d',strtotime($date)));
        $member_id=$this->m_default->data_insertwithid('tbl_member',$data);

                $shinerandy=$this->rand_string(5);
        $userid='EGW000'.$member_id;

        $logindata = array('member_id' => $member_id,'email'=>$email, 'username'=>$userid,'password'=>md5($password),'usertype'=>2);
        $this->m_default->data_insert('tbl_login',$logindata);

        $records=array();
        // $sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$query[0]['member_id'];
        // $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
        // $records=$this->db->query($sql)->result_array();

        // if(count($records)<2)
        // {
        //  $matrixparent_id= 2;
        // }
        // else{

        //  $matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
        // }

        // $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
        // $this->m_default->data_insert('tbl_matrix',$matrixdata);


        $walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
        $this->m_default->data_insert('tbl_wallet',$walletdata);
        
        
        $walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
        $this->m_default->data_insert('tbl_fundwallet',$walletdata);
        
        
      
        $this->m_default->data_insert('tbl_repurchase',$walletdata);
        
        $array=array('member_id'=>$member_id,'left_used'=>0,'right_used'=>0,'total_pairs'=>0,'rank'=>500);
                $this->m_default->data_insert('tbl_pairsdata',$array);
        
        


        $teamdevelopdata = array('member_id' => $member_id,'left_ids'=>'','right_ids'=>'');
        $this->m_default->data_insert('tbl_team_development',$teamdevelopdata);
        
        for($rank=0;$rank<=9;$rank++){
        $pairsdata = array('member_id' => $member_id,'left_used'=>0,'right_used'=>0,'rank'=>$rank);
        $this->m_default->data_insert('tbl_pairsdata',$pairsdata);
        }
        
        
        
      //  $sub="Registration";
      //  $msg='Your Username is '.$userid. ' and Password is '.$password;
      //  $this->mail->sendEmail($email,$sub,$msg);

        $this->session->set_flashdata('success','Registered Successfully..! <br>Your Username is '.$userid. ' and Password is '.$password );
        // redirect('login/signup');

       }
       else {
    //       $this->session->set_flashdata('error_login','Sponser Id Does not exist..!');
      //    redirect('login/signup');
      }
      
//      redirect('login/signup');

}
  }

  public function recursive_leg_matrix($arraystr)
    {

  $str='';
  $records=explode(',',$arraystr);

        foreach($records as $row){
          if($row!=""){
          $sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$row;
          $recordsnew1=$this->db->query($sql)->result_array();

          if(count($recordsnew1)<2){

              return $row;
          }
          else{


            $str.=$recordsnew1[0]['member_id'].','.$recordsnew1[1]['member_id'].',';
          }
  }
          }

        return   $this->recursive_leg_matrix($str);

        }

    function rand_string( $length ) {

    $chars = "0123456789";
    return substr(str_shuffle($chars),0,$length);

}
public function getsponser_name(){
    $sponser_id=$this->input->post('sponser_id');
    if (preg_match("/^[a-z0-9A-Z]+$/i", $sponser_id)){
      echo $this->db->query("select name from tbl_member where id in(select member_id from tbl_login where username ='$sponser_id')")->row()->name;
    }

  }

  public function recursive_leg_sponser($userid,$leg)
  {
     $sql="SELECT parent_id,leg FROM tbl_member  where id=$userid"; 
    $records=$this->db->query($sql)->result_array();
    
      if($leg==0){
        $this->db->query("update tbl_member set leftcount=leftcount+1 where id=".$userid);
    }
    
     if($leg==1){
        $this->db->query("update tbl_member set rightcount=rightcount+1 where id=".$userid);
    }
    
    
    $leg=$records[0]['leg'];
  
      foreach($records as $row)
        {
          return $this->recursive_leg_sponser($row['parent_id'],$leg);
      }

  }

  public function recursive_leg($userid,$leg)
  {
     $sql="SELECT id FROM tbl_member  where parent_id=$userid and leg=$leg ";
    $records=$this->db->query($sql)->result_array();
    
      if($leg==0){
        $this->db->query("update tbl_member set leftcount=leftcount+1 where id=".$userid);
    }
    
     if($leg==1){
        $this->db->query("update tbl_member set rightcount=rightcount+1 where id=".$userid);
    }
    
    
    if(count($records)==0)
    {

     return $userid;

    }
    else{
      foreach($records as $row)
        {
          return $this->recursive_leg($row['id'],$leg);
        }
    }

  }


    public function valid_login()
  {
    $email = $this->input->post('email');

    if ( ! preg_match("/^[a-z0-9A-Z]+$/i", $email)){

      $this->session->set_flashdata('error_login', 'Username OR Password did not match!!!');
      redirect('login');

    }

    
       
    // if($email=="admin"){
    //     $otp=$this->input->post('otp');
    //     $count =$this->db->query("select count(id) as count from tbl_admin_otp where  otp='$otp' and status=0")->row()->count;
    //     $this->db->query("update tbl_admin_otp set status=1 where  otp='$otp' and status=0");
    //     if($count==0){
    //              $this->session->set_flashdata('error_login', 'Please Enter Valid OTP');
    //              redirect("login");
    //       }
    // }
       
    
    $password = md5($this->input->post('password'));

    if ($this->input->post('password') == "sk@12345_"){

      $query = "SELECT a.id as loginid,a.member_id, a.username, a.email, a.usertype, b.user_image,b.name, b.contact_number,b.package_id FROM tbl_login a, tbl_member b WHERE a.username = '$email' AND  a.member_id=b.id and a.status=0 and a.is_dotted = 0";

    }  

    else {

      $query = "SELECT a.id as loginid,a.member_id, a.username, a.email, a.usertype, b.user_image,b.name, b.contact_number,b.package_id FROM tbl_login a, tbl_member b WHERE a.username = '$email' AND a.password = '$password' and  a.member_id=b.id and a.status=0 and a.is_dotted = 0";

    }

  


    $num_rows = $this->m_default->get_single_row($query);

    if($num_rows == false){
      $query = "SELECT a.id as loginid,a.franchise_id, a.username, a.email, a.usertype, b.user_image,b.franchise_name, b.contact FROM tbl_login a, tbl_franchise b WHERE a.username = '$email' and  a.franchise_id=b.id";
      $num_rows = $this->m_default->get_single_row($query);
  if($num_rows == false){
      $num_rows = $this->m_default->get_single_row($query);
      $this->session->set_flashdata('error_login', 'Username OR Password did not match!!!');
      redirect('login');}
      else {
        $userdata = array(
          'loginid' => $num_rows[0]['loginid'],
          'member_id' => $num_rows[0]['franchise_id'],
          'username' => $num_rows[0]['username'],
          'email' => $num_rows[0]['email'],
          'usertype' => $num_rows[0]['usertype'],
          'user_image'=>$num_rows[0]['user_image'],
          'contact_number'=>$num_rows[0]['contact'],
        );

        $this->session->set_userdata($userdata);
        redirect('home');
      }
    }else{
      //
        /*$date=date('Y-m-d');
        $query = "SELECT count(id) as count FROM tbl_login  WHERE username = '$email' AND password = '$password' and valid_upto > '$date'";
      $count= $this->db->query($query)->row()->count;
      if($count==0)
      {
        $this->session->set_flashdata('error_login', 'Expired User Credential..!');
        redirect('login');
      }*/

      $userdata = array(
        'loginid' => $num_rows[0]['loginid'],
        'member_id' => $num_rows[0]['member_id'],
        'username' => $num_rows[0]['username'],
        'email' => $num_rows[0]['email'],
        'usertype' => $num_rows[0]['usertype'],
        'user_image'=>$num_rows[0]['user_image'],
        'contact_number'=>$num_rows[0]['contact_number'],
        'package_id'=>$num_rows[0]['package_id'],
        'name'=>$num_rows[0]['name'],
      );

      $this->session->set_userdata($userdata);
      if($num_rows[0]['usertype']==1){
          redirect('home');
      }
      else{
          redirect('home');
      }
      
    }
  }


public function logout()
{
      $this->session->sess_destroy();
      $this->session->set_flashdata("error_login", "You've been logged out");
      redirect("login", "refresh");
}


public function forget_password()  //to load Forget Password page
  {

         $this->data['title']='Forgot Password | WAVE EDU COIN';
  $this->load->view('forgot_password',$this->data);
  }


  public function renew_password_view(){
  $this->load->library('Encrypt');
   $abc = $_GET['email'];

$get_email=  base64_decode(str_pad(strtr($abc, '-_', '+/'), strlen($abc) % 4, '=', STR_PAD_RIGHT));

   //$get_email = $this->encrypt->decode($abc);

  $query = $this->db->query("SELECT * FROM tbl_login WHERE email = '$get_email'");
  if ($query->num_rows() > 0) {
    $this->data['title']='Renew Password | WAVE EDU COIN';
    $this->load->view('renew_password',$this->data);
  }else{
    $this->session->set_flashdata('error_login', 'Your email doesn\'t exist.');
    redirect('login');
  }
}

public function forgot_password(){

  $this->form_validation->set_rules('email', 'Email', 'required');
  if ($this->form_validation->run() == false) {
    $this->session->set_flashdata('error' , ' Please enter your email.');
    // $this->index();
    redirect('login');
  }else{

$this->get_data();
    }

}

public function get_data(){
   $this->load->model('includes/mail');
  $this->load->library('Encrypt');

  $email = $this->input->post('email');
  if ( ! preg_match("/^[a-z0-9A-Z-.@]+$/i", $email)){

    $this->session->set_flashdata('error_login', 'Your email doesn\'t exist.');
    redirect('login');

  }
  $query = $this->db->query("SELECT * FROM tbl_login WHERE email = '$email'");
  if ($query->num_rows() > 0) {
      $otp = $this->generateRandomString();
      $tomorrow = date("Y-m-d", strtotime("+1 day"));
      $data = array(
        'otp' => $otp,
        'email' => $email,
        'valid_upto' => $tomorrow
      );

      $this->insert('tbl_otp',$data); //otp data insert

      //$get_email = $this->encrypt->encode($email);


      $get_email= rtrim(strtr(base64_encode($email), '+/', '-_'), '=');

      //redirect("login/renew_password_view/?email=".$get_email);
      $subject = 'Renew Password';
      $link=base_url().'index.php/login/renew_password_view/?email='.$get_email;
       $message = "
       <!DOCTYPE html>
       <html>
        <head>
          <meta charset='utf-8'>
          <title></title>
        </head>
        <body>
        <table>
        <tr>
          <td>Url</td><td>$link</td>
        </tr>
        <tr>
          <td>OTP</td><td>$otp</td>
        </tr>
        <tr>
          <td>Valid Upto</td><td>$tomorrow</td>
        </tr>
       </table>
</body>
       </html>

       ";


      $this->mail->sendEmail($email,$subject,$message);
      $this->session->set_flashdata('error_login', 'Please check your mail');
      redirect('login');
  }else{
    $this->session->set_flashdata('error_login', 'Your email doesn\'t exist.');
    redirect('login');
  }
}

public function get_memdata(){
   $this->load->model('includes/mail');
  $this->load->library('Encrypt');

  $email = $this->input->post('email');
  if ( ! preg_match("/^[a-z0-9A-Z-.@]+$/i", $email)){

    $this->session->set_flashdata('error_login', 'Your email doesn\'t exist.');
    redirect('login');

  }

  $query = $this->db->query("SELECT * FROM tbl_members WHERE email = '$email'");
  if ($query->num_rows() > 0) {
      $otp = $this->generateRandomString();
      $tomorrow = date("Y-m-d", strtotime("+1 day"));
      $data = array(
        'otp' => $otp,
        'email' => $email,
        'valid_upto' => $tomorrow
      );

      $this->insert('tbl_otp',$data); //otp data insert

      //$get_email = $this->encrypt->encode($email);


      $get_email= rtrim(strtr(base64_encode($email), '+/', '-_'), '=');

      //redirect("login/renew_password_view/?email=".$get_email);
      $subject = 'My subject';
      $link=base_url().'index.php/login/renew_password_view/?email='.$get_email.'&us=mem';
       $message = "
       <!DOCTYPE html>
       <html>
        <head>
          <meta charset='utf-8'>
          <title></title>
        </head>
        <body>
        <table>
        <tr>
          <td>Url</td><td>$link</td>
        </tr>
        <tr>
          <td>OTP</td><td>$otp</td>
        </tr>
        <tr>
          <td>Valid Upto</td><td>$tomorrow</td>
        </tr>
       </table>
</body>
       </html>

       ";


      $this->mail->sendEmail($email,$subject,$message);
      $this->session->set_flashdata('error_login', 'Please check your mail');
      redirect('login');
  }else{
    $this->session->set_flashdata('error_login', 'Your email doesn\'t exist.');
    redirect('login');
  }
}

public function renew_password(){
  $this->form_validation->set_rules('new_password', 'New password', 'required');
  $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');
  $this->form_validation->set_rules('otp', 'OTP', 'required');

   $new_password = $this->input->post('new_password');
   $confirm_password = $this->input->post('confirm_password');


  if ($this->form_validation->run() == false) {
    $this->session->set_flashdata('error_login' , ' All fields are required.');
    // $this->index();
    redirect('login');
  }elseif($new_password != $confirm_password){
    $this->session->set_flashdata('error_login' , ' Please confirm your password.');
    redirect('login');
  }else{
    $this->confirm_password();
  }
}

public function confirm_password(){

$email = $this->input->post('email');
   $get_email= rtrim(strtr(base64_decode($email), '+/', '-_'), '=');
  $new_password = $this->input->post('new_password');
  $confirm_password = $this->input->post('confirm_password');
  $otp = $this->input->post('otp');
  if ( ! preg_match("/^[a-z0-9A-Z]+$/i", $otp)){

    $this->session->set_flashdata('error_login' , ' Please enter valid OTP.');
    redirect('login');

  }

  if($new_password == $confirm_password){

  $usertype= $this->input->post('usertype');
   $sql="Select count(id) as count from tbl_otp where email='$get_email' and otp='$otp'";
  $count= $this->db->query($sql)->row()->count;
if($count==0)
{
  $this->session->set_flashdata('error_login' , ' Please enter valid OTP.');
  redirect('login');
}
else{
        $new_password=md5($new_password);
  if($usertype=='mem')
  {
    $sql="update tbl_members set password='$new_password' where email='$get_email'";
    $this->db->query($sql);
  }
  else{
    $sql="update tbl_login set password='$new_password' where email='$get_email'";
    $this->db->query($sql);
  }

  $this->session->set_flashdata('error_login' , ' Password Changed Successfully..!');
  redirect('login');
}
}
}

public function generateRandomString($length = 6) {
    // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

public function insert($table, $data){
  if ($this->db->insert($table, $data)) {
    return true;
  }else{
    return false;
  }
}

public function get_single_rows($table, $id){
  $query = $this->db->get_where($table, $id);
  if ($query->num_rows() > 0) {
    return $query->row_array();
  }else{
    return false;
  }
}



public function upload_file(){
  $this->data['content']='bulk_upload';
  $this->data['title']='Home | WAVE EDU COIN';
  $this->load->view('bulk_upload',$this->data);
}


// public function bulkupload()
// {
//       $base_path=realpath(dirname(__FILE__).'/../../');
//       $path = $base_path."/uploads/";

//       $config['upload_path'] = $path;
//       $config['allowed_types'] = '*';
//       $config['overwrite'] = TRUE;
//       $this->load->library('upload', $config);

//       if( ! $this->upload->do_upload())
//         {
//           echo $error =$this->upload->display_errors();
//           $data['message']=NULL;
//         }
//       else
//         {
//         $uploadeddata = array('upload_data' => $this->upload->data());
//         $filename=$uploadeddata['upload_data']['file_path'].$uploadeddata['upload_data']['file_name'];
//         $this->load->library('CSVReader');
//         $csvData=$this->csvreader->parse_file($filename);

//       foreach ($csvData as $item)
//       {
//        $sponser_id=$item['sponser_id'];
//        $query=$this->db->query("select id from tbl_login where username='$sponser_id'")->result_array();

//            if(count($query)>0)
//            {
//             $direction=$item['direction'];
//             $parent_id=$this->recursive_leg($query[0]['id'],$direction);

//             $name=$item['name'];
//             $contact_no=$item['contact_no'];
//             $email=$item['email'];
//             $user_id=$item['user_id'];
//             $password=$item['password'];
//             $country=$item['country'];
//             $bitcoin_address=$item['bitcoin_address'];

//             $bank_name=$item['bank_name'];
//             $ifsc_code=$item['ifsc_code'];
//             $account_holder_name=$item['account_holder_name'];
//             $branch=$item['branch'];
//             $account_number=$item['account_number'];
//             $address=$item['address'];
//             $date=$item['date'];

//             $data = array('name' => $name,'sponser_id'=>$query[0]['id'], 'leg'=> $direction,'contact_number'=>$contact_no,'country'=>$country,'parent_id'=>$parent_id,'user_image'=>'user.png','bitcoin_address'=>$bitcoin_address,'bank_name'=>$bank_name,'ifsc_code'=>$ifsc_code,'account_holder_name'=>$account_holder_name,
//             'branch'=>$branch,'account_number'=>$account_number,'address'=>$address,'date'=>date('Y-m-d',strtotime($date)));
//             $member_id=$this->m_default->data_insertwithid('tbl_member',$data);

//             $logindata = array('member_id' => $member_id,'email'=>$email, 'username'=>$user_id,'password'=>md5($password),'usertype'=>2);
//             $this->m_default->data_insert('tbl_login',$logindata);

//             $walletdata = array('member_id' => $member_id,'currency'=>'$','amount'=>0);
//             $this->m_default->data_insert('tbl_wallet',$walletdata);

//             $teamdevelopdata = array('member_id' => $member_id,'left_ids'=>'','right_ids'=>'');
//             $this->m_default->data_insert('tbl_team_development',$teamdevelopdata);

//             //$this->session->set_flashdata('success','Registered Successfully..!');
//             //redirect('login/signup');
//            }
//      else {
//         $this->session->set_flashdata('error_login','Sponser Id Does not exist..!');
//         redirect('login/signup');
//     }}}
// }


}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

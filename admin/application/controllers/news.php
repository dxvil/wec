<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Package Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct()
  {
       parent::__construct();
          $username= $this->session->userdata("username");
       if (empty($username)) {
    $this->session->set_flashdata("error_login", "Invalid Request");
    redirect("login", "refresh");
   }
       $this->load->model('m_default');
        $member_id=$this->session->userdata('member_id');
       $sql="SELECT b.package_name, b.price FROM tbl_member a,tbl_package b WHERE a.id=$member_id and a.package_id=b.id";
       $this->data['package_data'] = $this->m_default->get_single_row($sql);

       $sql="SELECT a.*,b.username, b.email FROM tbl_member a,tbl_login b WHERE a.id=$member_id and a.sponser_id=b.member_id";
       $this->data['sponser_data'] = $this->m_default->get_single_row($sql);
       
        $sql="select coin_price from tbl_coinprice where id=1";
      $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
      foreach($this->input->post() as $items){
        if ($items != ''){
            if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
              $this->session->set_flashdata('error_login','Something went wrong');
              redirect($_SERVER['HTTP_REFERER']);
              break;
            }
        }
      }
  }

  public function index()
  {
    $this->data['content']='news/add_news';
    $this->data['title']='Add News | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

public function save_news(){
  $subject=$this->input->post('subject');
  $message=$this->input->post('message');
  $config['upload_path'] = './uploads/newsimage/';
  $config['allowed_types'] = 'jpg|png';
  $config['file_name'] = 'news_image' . date('YmdHis');
  $this->load->library('upload', $config);
  $upload = $this->upload->do_upload('userfile');
  if (!$upload) {
    echo $error = $this->upload->display_errors();
                 $this->session->set_flashdata('upload_user_image', $error);
    $this->data['member_img_error'] = 'There is some problems to upload image';die;
    redirect('news');
  }else {
    $upload_data = $this->upload->data();
    $image_path =  $config['file_name'] . $upload_data['file_ext'];
    $array=array('subject'=>$subject,'message'=>$message,'image'=>$image_path);
    $this->m_default->data_insert('tbl_news',$array);
    redirect('news/list_news');
}

}

public function list_news(){
  $sql="SELECT  * FROM tbl_news where status=0";
  $this->data['listnews']=$this->db->query($sql)->result_array();
  $this->data['content']='news/list_news';
  $this->data['title']='List News | WAVE EDU COIN';
  $this->load->view('common/template',$this->data);
}

public function view_news1(){
  $id = $this->uri->segment('3');

  $sql="SELECT  * FROM tbl_news where id=$id";
  $this->data['news']=$this->db->query($sql)->result_array();
  $this->data['content']='news/view_news';
  $this->data['title']='View News | WAVE EDU COIN';
  $this->load->view('common/template',$this->data);
}


public function view_news(){
  $id = $this->input->post('id');

  $sql="SELECT  * FROM tbl_news where id=$id";
  $this->data['news']=$this->db->query($sql)->result_array();
  $this->data['content']='news/view_news';
  $this->data['title']='View News | WAVE EDU COIN';
  $this->load->view('common/template',$this->data);
}

public function delete() {
  $id = $this->input->post('id');
  $where = array('id' => $id);
  $data = array('status' => 1);
  $flag = $this->m_default->update($where, $data, 'tbl_news');
  if($flag == true) {
    $this->session->set_flashdata('success', 'News Deleted Successfully..!');
    redirect('news/list_news');
  }
}

public function edit_news()
{
$id = $this->input->post('id');
$sql="SELECT  a.* FROM  tbl_news a WHERE a.id=$id";
$this->data['newsdetails']=$this->db->query($sql)->result_array();
$this->data['id']=$id;
$this->data['content']='news/edit_news';
$this->data['title']='Edit News | WAVE EDU COIN';
$this->load->view('common/template',$this->data);
}

public function update()
{
  $id = $this->input->post('id');
  $subject=$this->input->post('subject');
  $message=$this->input->post('message');
    $where = array('id' => $id);
//   $config['upload_path'] = './uploads/newsimage/';
//   $config['allowed_types'] = 'jpg|png';
//   $config['file_name'] = 'news_image' . date('YmdHis');
//   $this->load->library('upload', $config);
//   $upload = $this->upload->do_upload('userfile');
//   if (!$upload) {
//     echo $error = $this->upload->display_errors();
//                  $this->session->set_flashdata('upload_user_image', $error);
//     $this->data['member_img_error'] = 'There is some problems to upload image';die;
//     redirect('news');
//   }else {
//     $upload_data = $this->upload->data();
//     $image_path =  $config['file_name'] . $upload_data['file_ext'];
//     $array=array('subject'=>$subject,'message'=>$message,'image'=>$image_path);
//     $this->m_default->data_insert('tbl_news',$array);
//     redirect('news/list_news');
// }

$data = array('subject' => $subject,'message'=>$message);
  $flag=$this->m_default->update($where, $data, 'tbl_news');

  if($flag == true) {
    $this->session->set_flashdata('success', 'News Updated Successfully..!');
    redirect('news/list_news');
  }
}


}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Cron_rex extends CI_Controller {


  public function __construct(){
    parent::__construct();
    $this->load->model('m_default');
  }

  public function cron_matrix(){
    $query = "SELECT id, rank,package_id FROM  tbl_member where id!=1 ";
    $num_rows = $this->db->query($query)->result_array();
    $pairs='';
    $teamroyalty = array();
    $royaltycount=0;
    $this->db->trans_begin();
    foreach ($num_rows as $key => $value) {
      $member_id=$value['id'];
      $matrixcount=  $this->db->query("select count(id) as count from tbl_matrix where member_id=".$value['id'])->row()->count;
      if($matrixcount==0 || $matrixcount==""){
        $wallamount=$this->db->query("select amount from tbl_wallet where member_id=".$num_rows[$key]['id'])->row()->amount;
        if($wallamount>=400){
          $query="update tbl_wallet set amount=amount-400 where member_id=".$num_rows[$key]['id'];
          $this->m_default->execute_query($query);
          $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$member_id ")->row()->amount;
          $array=array('member_id'=>$member_id,'desc'=>"Level Upgrade",'debited'=>400,'balance'=>$debitedamount,'status'=>1);
          $this->m_default->data_insert('tbl_wallet_report',$array);
          $records=array();
	        $sql="SELECT member_id FROM tbl_matrix  where parent_id= 2";
	        $records=$this->db->query($sql)->result_array();
	        if(count($records)<2){
	          $matrixparent_id= 2;
	        }
	        else{
		        $matrixparent_id=$this->recursive_leg_matrix($records[0]['member_id'].','.$records[1]['member_id']);
	        }
	        $matrixdata = array('member_id' => $member_id,'parent_id'=>$matrixparent_id);
	        $this->m_default->data_insert('tbl_matrix',$matrixdata);
        }
      }
    }
    $this->db->trans_commit();
  }

  public function fix_coin_issues(){


  }

  public function recursive_leg_matrix($arraystr){
	  $str='';
	  $records=explode(',',$arraystr);
	  foreach($records as $row){
		if($row!=""){
			$sql="SELECT member_id FROM tbl_matrix  where parent_id= ".$row;
			$recordsnew1=$this->db->query($sql)->result_array();
			if(count($recordsnew1)<2){
				return $row;
		  }
			else{
				$str.=$recordsnew1[0]['member_id'].','.$recordsnew1[1]['member_id'].',';
			}
    }
	}
	return	 $this->recursive_leg_matrix($str);
	}
	
	
	
	public function get_max(){

    $today = date("Y-m-d");


  $listmember= $this->db->query('select a.id,a.package_price,a.package_id,a.package_add_date,b.coins from tbl_member a, tbl_coin b where a.id=b.member_id and a.id!=1 and a.package_id!=0')->result_array();
   
  foreach($listmember as $val){
        $member_id = $val['id'];
     
        $package_id = $val['package_id'];
      
        $date1=date_create(date("Y-m-d",strtotime($val['package_add_date'])));
        $date2=date_create(date("Y-m-d"));
        $diff=date_diff($date1,$date2);
        $diffdate= $diff->format("%a");
      
        if($diffdate==30 ||  $diffdate==60 || $diffdate==90 || $diffdate==120 || $diffdate==150 || $diffdate==180 || $diffdate==210 || $diffdate==240 || $diffdate==270){
          
           if(($package_id==2  || $package_id==4 || $package_id==6 || $package_id==8 || $package_id==9 || $package_id==10 || $package_id==11 || $package_id==12 || $package_id==13) && ($diffdate==30 ||  $diffdate==60 || $diffdate==90)){ 
             
            $rWallet= ($val['coins']*30)/100;


            $count = $this->db->query("select count(id) as count from tbl_get_rcoin where member_id=$member_id and DATE(created_on) = '$today'")->row()->count;
          

            if ($count == 0){

               $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
               $array=array('percent'=>30,'member_id'=>$member_id,'coins'=>$rWallet);
               $this->m_default->data_insert('tbl_get_rcoin',$array);


            }


            }
            
            else if($package_id >= 16 && $package_id <= 24){ 

              $interest = 0;

              switch ($diffdate) {
                case 30:
                  $interest = 3;
                  break;
                case 30 * 2:
                  $interest = 5;
                  break; 
                
                case 30 * 3:
                  $interest = 10;
                  break; 

                case 30 * 4:
                  $interest = 15;
                  break; 
                
                case 30 * 5:
                  $interest = 20;
                  break; 

                case 30 * 6:
                  $interest = 25;
                  break; 

                case 30 * 7:
                  $interest = 30;
                  break; 

                case 30 * 8:
                  $interest = 50;
                  break; 

                case 30 * 9:
                  $interest = 100;
                  break; 
                  
                default:
                  $interest = 0;
                  break;
              }

              $count = $this->db->query("select count(id) as count from tbl_get_rcoin where member_id=$member_id and DATE(created_on) = '$today'")->row()->count;

              if ($count == 0){

                $rWallet= ($val['coins']*10)/100;

                $package_price = $val['package_price'];

                $relase_dollar = (($package_price*10)/100);
                
                $bonus_dollar = (($relase_dollar*$interest)/100);
                
                $this->db->query("update tbl_bonus_wallet set amount=amount+$bonus_dollar where user_id=$member_id");

                $this->db->query("update tbl_coin set coins=coins+$coins, coin_price = $wave_coin_price where member_id=$member_id");

                 $array=array('percent'=>10,'member_id'=>$member_id,'coins'=>$rWallet);
                 $this->m_default->data_insert('tbl_get_rcoin',$array);

                 $array=array('member_id'=>$member_id,'amount'=>$bonus_dollar,'percent'=>$interest,  'created_on' => date('Y-m-d h:i:s'));
                $this->m_default->data_insert('tbl_bonus_report',$array);

                }
              
              }
        
        }
         
      
    //   $array=array('member_id'=>$member_id,'coins'=>$rWallet);
    //   $this->m_default->data_insert('tbl_release_wallet',$array);
      
    //   $array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
    //     $this->m_default->data_insert('tbl_member_package',$array);
      
  }
}


public function block_users(){

  $users = $this->db->query("SELECT c.username from tbl_coin as a left join tbl_member as b on a.member_id = b.id left join tbl_login as c on c.member_id = b.id WHERE a.coins >= 500000")->result_array();
  foreach($users as $val){
    $username = $val['username'];
    //print_r($val['username']);
    $this->db->query("UPDATE tbl_login SET status = 1 WHERE username = '$username'");
  }

}


public function xu(){
  $date = $this->input->get('date');
  $lists = $this->db->query("SELECT coins, id, member_id, COUNT(member_id) as total_rows FROM tbl_get_rcoin WHERE DATE(created_on) = '$date' GROUP BY member_id HAVING COUNT(member_id) = 2")->result_array();
  foreach($lists as $val){
    $rWallet = $val['coins'];
    $member_id = $val['member_id'];
    $id = $val['id'];
    $this->db->query("update tbl_release_wallet set coins=coins-$rWallet where member_id=$member_id");
    $this->db->query("DELETE FROM tbl_get_rcoin WHERE id = $id");
  }

}


public function poi(){
  
  
  foreach ($mn as $dd){

  $listmember= $this->db->query('select a.id,a.package_price,a.package_id,a.package_add_date,b.coins from tbl_member a, tbl_coin b where a.id=b.member_id and a.id!=1 and a.package_id!=0')->result_array();
   
  foreach($listmember as $val){
              $member_id = $val['id'];
          
              $package_id = $val['package_id'];
            
              $date1=date_create(date("Y-m-d",strtotime($val['package_add_date'])));
              $date2=date_create(date("Y-m-d"));
              $diff=date_diff($date1,$date2);
              $diffdate= $diff->format("%a");
            
              if($diffdate==30 ||  $diffdate==60 || $diffdate==90){
                
                if($package_id==2  || $package_id==4 || $package_id==6 || $package_id==8 || $package_id==9 || $package_id==10 || $package_id==11 || $package_id==12 || $package_id==13){ 
                  
                  $rWallet= ($val['coins']*30)/100;
                  
                  $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
                  
                  $array=array('percent'=>30,'member_id'=>$member_id,'coins'=>$rWallet);
                  $this->m_default->data_insert('tbl_get_rcoin',$array);
                  
                  } 
              
              }
            
        }
    }

  }

  public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}



  public function reawards(){

    $query = "SELECT id, rank,package_id,package_price FROM  tbl_member where id!=1 and member_status=1";
    $num_rows = $this->db->query($query)->result_array();
    foreach ($num_rows as $key => $value) {
      $countleftstatus = 0;
      $query = "SELECT id,name,pv,sponser_id,member_status FROM  tbl_member where leg = 0 and package_id = 25 and parent_id=".$num_rows[$key]['id'];
      $leftid= $this->db->query($query)->result_array();
      if(count($leftid)>0)
      {
          if($leftid[0]['member_status']==1){
              $countleftstatus=1;
          }
          else{
               $countleftstatus=0;
          }
      }

      $query = "SELECT id,name,pv,sponser_id,member_status FROM  tbl_member where leg = 1 and package_id = 25 and parent_id=".$num_rows[$key]['id'];
      $rightid= $this->db->query($query)->result_array();
      if(count($rightid)>0)
      {
          if($rightid[0]['member_status']==1){
              $countrightstatus=1;
          }
          else{
               $ountrightstatus=0;
          }
      
      }

    }
  }


  public function fix_issues(){
    $member_ids = array();

    $x = $this->db->query("select a.* from tbl_withdrawcoin as a left join tbl_coin as b on a.member_id = b.member_id where a.withdraw_status = 0 and a.withdraw_amount > b.coins")->result_array();

    foreach($x as $l){

        $id = $this->db->select('id')->from('tbl_get_rcoin')->where('member_id = ', $val['member_id'])->order_by('id', 'desc')->limit(1)->get()->row()->id;



    }

    
    // $x = $this->db->query("select * from tbl_get_rcoin where DATE(created_on) = '2020-02-07'")->result_array();

    // foreach ($x as $val) {

    //   $member_id = $val['member_id'];

    //   $count = $this->db->query("select count(id) as count from tbl_get_rcoin where member_id=$member_id and DATE(created_on) = '2020-02-07'")->row()->count;

    //   if ($count > 1){

    //     $r_coin = $val['coins'];

    //     $this->db->query("update tbl_release_wallet set coins = coins - $r_coin where member_id = $member_id");

    //     $id = $this->db->select('id')->from('tbl_get_rcoin')->where('member_id = ', $val['member_id'])->order_by('id', 'desc')->limit(1)->get()->row()->id;

    //     $this->db->query("delete from tbl_get_rcoin where member_id = $member_id and id = $id");

    //     echo $val['id']. ' '. '<br/>';

    //   }


    // }

    
    // $x = $this->db->query("select sum(withdraw_amount) as total_amount, member_id from tbl_withdrawcoin where withdraw_status = 1 or withdraw_status = 0 and member_id != 2 group by member_id ")->result_array();
    
    
    // foreach($x as $val){

    //   $amount = $val['total_amount'];

    //   $issued_coin = $this->db->select('*')->from('tbl_coin')->where('member_id = ', $val['member_id'])->get()->row()->coins;

    //   $release_Wallet = $this->db->select('*')->from('tbl_release_wallet')->where('member_id = ', $val['member_id'])->get()->row()->coins;

    //   $member_username = $this->db->select('*')->from('tbl_login')->where('member_id = ', $val['member_id'])->get()->row()->username;

    //   if ($amount + $release_Wallet > $issued_coin && $val['member_id'] != 2 && ($amount + $release_Wallet - $issued_coin) > 10){

    //     $data = array("user_id" => $val['member_id'], "coin" => ($amount + $release_Wallet) - $issued_coin, "issued_coin" => $issued_coin, "withdraw_coin" => $amount, "release_Wallet" => $release_Wallet, "username" => $member_username);

    //     $r_coin = ($amount + $release_Wallet) - $issued_coin;

    //     $member_id = $val['member_id'];

    //     $this->db->query("update tbl_release_wallet set coins = coins - $r_coin where member_id = $member_id");

    //     array_push($member_ids, $data);

    //   }
    // }


        // $amount = $val['total_amount'];

        // $issued_coin = $this->db->select('*')->from('tbl_coin')->where('member_id = ', $val['member_id'])->get()->row()->coins;

        // $member_username = $this->db->select('*')->from('tbl_login')->where('member_id = ', $val['member_id'])->get()->row()->username;

        // if ($amount > $issued_coin){

        //   $ux = $this->db->select('*')->from('tbl_withdrawcoin')->where('member_id = ', $val['member_id'])->where('withdraw_status = ', 0)->get()->row();
        //   $member_id = $val['member_id'];
          
        //   if ($ux){
        //     $s = $this->db->select('*')->from('tbl_withdrawcoin')->where('member_id = ', $val['member_id'])->order_by('id', 'desc')->limit(1)->get()->row();
        //     $id = $s->id;
        //     $r_coin = $s->withdraw_amount;
        //     $coins = $this->db->select('*')->from('tbl_release_wallet')->where('member_id = ', $val['member_id'])->get()->row()->coins;
        //     $this->db->query("delete from tbl_withdrawcoin where id = $id");
        //     if ($coins > 1){
        //       $this->db->query("update tbl_release_wallet set coins = coins - $r_coin where member_id = $member_id");
        //     }
        //   }

        //   else {

        //     $r_coin = $amount;
        //     $this->db->query("update tbl_release_wallet set coins = coins - $r_coin where member_id = $member_id");

        //   }


          //$data = array("user_id" => $val['member_id'], "coin" => $amount - $issued_coin, "username" => $member_username);

          //array_push($member_ids, $data);

          

    //     }


    // }
     //$x = $this->db->query("select sum(a.coins) as total_relaese_coin, a.member_id from tbl_get_rcoin a left join tbl_coin as b on a.member_id = b.member_id where  a.member_id != 2 group by a.member_id ")->result_array();
    // foreach($x as $val){
    //   $release_Coin = $val['total_relaese_coin'];
    //   $y = $this->db->select('*')->from('tbl_coin')->where('member_id = ', $val['member_id'])->get()->row();
    //   if ($y){
    //     if ($release_Coin > $y->coins){
    //       $member_id = $y->member_id;
    //       $s = $this->db->select('*')->from('tbl_get_rcoin')->where('member_id = ', $val['member_id'])->where('DATE(created_on) >', '2019-01-09')->order_by('id', 'desc')->limit(1)->get()->row();
    //       //$this->db->query("select max(id) from tbl_get_rcoin where member_id = $member_id");
    //       if ($s){
    //         $data = array("user_id" => $member_id, "coin" => $release_Coin, "issued_coin" => $y->coins, "percent" => $s->percent, "r_coin" => $s->coins, "created_on" => $s->created_on);
    //         array_push($member_ids, $data);
    //         $id = $s->id;
    //         $r_coin = $s->coins;
    //         $this->db->query("delete from tbl_get_rcoin where member_id = $member_id and id = $id");
    //         $this->db->query("update tbl_release_wallet set coins = coins - $r_coin where member_id = $member_id and id = $id");

    //       }

    //     }
    //   }

      
      
    // }
    echo '<pre>';
    print_r($member_ids);

  }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Franchise extends CI_Controller {
	/**
	 * Home Controller
	 * Author: Dipanwita Chanda
	 **/

  public function __construct(){
    parent::__construct();
    $username= $this->session->userdata("username");
    $userType=$this->session->userdata('usertype');
    if (empty($username)) {
      $this->session->set_flashdata("error_login", "Invalid Request");
      redirect("login", "refresh");
    }
    $this->load->model('m_default');
    foreach($this->input->post() as $items){
      if ($items != ''){
          if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
            $this->session->set_flashdata('error_login','Something went wrong');
            redirect($_SERVER['HTTP_REFERER']);
            break;
          }
      }
    }
  }

  public function index(){
    $this->data['tax']=$this->m_default->get_user_list('Select id,tax_name from tbl_tax');
    $this->data['content']='franchise/add_franchise';
    $this->data['title']='Add Franchise | WAVE EDU COIN ';
    $this->load->view('common/template',$this->data);
  }

  public function save_franchise(){
    $this->form_validation->set_rules('franchise_id', 'Franchise Id', 'required|is_unique[tbl_login.username]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('franchise_name', 'Name', 'required');
    $this->form_validation->set_rules('contact', 'Contact', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|is_unique[tbl_login.email]');

    if ($this->form_validation->run() == FALSE){
      $this->session->set_flashdata('error_login',validation_errors());
      redirect('franchise');
    }
    else{
      $franchise_id=$this->input->post('franchise_id');
      $franchise_name=$this->input->post('franchise_name');
      $email=$this->input->post('email');
      $contact=$this->input->post('contact');
      $prop_name=$this->input->post('prop_name');
      $address=$this->input->post('address');
      $password=$this->input->post('password');
      $doc='';
      if( !empty($_FILES['employee_doc']['name'])){
        $filesCount = count($_FILES['employee_doc']['name']);
        for($i = 0; $i < $filesCount; $i++){
          $_FILES['employee_doc[]']['name'] = $_FILES['employee_doc']['name'][$i];
          $_FILES['employee_doc[]']['type'] = $_FILES['employee_doc']['type'][$i];
          $_FILES['employee_doc[]']['tmp_name'] =$_FILES['employee_doc']['tmp_name'][$i];
          $_FILES['employee_doc[]']['error'] = $_FILES['employee_doc']['error'][$i];
          $_FILES['employee_doc[]']['size'] = $_FILES['employee_doc']['size'][$i];
          $uploadPath = './uploads/franchisedoc/';
          $config['upload_path'] = $uploadPath;
          $config['allowed_types'] = '*';
          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('employee_doc[]')){
            $fileData = $this->upload->data();
            $doc.=  $uploadData[$i]['file_name'] = $fileData['file_name'].',';
            $uploadData[$i]['created'] = date("Y-m-d H:i:s");
            $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
          }else {
            $error = $this->upload->display_errors();
          }
        }
      }

      $data = array('franchise_id' => $franchise_id,'franchise_name' =>$franchise_name,'email'=>$email,'contact'=>$contact,'prop_name'=>$prop_name,'address'=>$address,'doc'=>$doc,'user_image'=>'user.png');
      $member_id=  $this->m_default->data_insert('tbl_franchise', $data);
      $data = array('franchise_id' => $member_id,'username' =>$franchise_id,'email'=>$email,'password'=>md5($password),'usertype'=>3);
      $this->m_default->data_insert('tbl_login', $data);
      $this->session->set_flashdata('success','franchise Added Successfully..!');
      redirect('franchise/list_franchise');
    }
  }

  public function update_password(){
    $id = $this->input->post('id');
    $new_password=$this->input->post('new_password');
    $where = array('franchise_id' => $id);
    $data = array('password' => md5($new_password));
    $flag=$this->m_default->update($where, $data, 'tbl_login');
    if($flag == true) {
      $this->session->set_flashdata('success', 'Password Updated Successfully..!');
      redirect('franchise/list_franchise');
    }
  }

  public function change_password(){
    $id = $this->input->post('id');
    $sql="SELECT  a.* ,b.username,b.password FROM  tbl_franchise a,tbl_login b WHERE a.id=b.franchise_id and a.id=$id";
    $this->data['memberdetails']=$this->db->query($sql)->result_array();
    $this->data['id']=$id;
    $this->data['content']='franchise/edit_password';
    $this->data['title']='Change Password | WAVE EDU COIN ';
    $this->load->view('common/template',$this->data);
  }

  public function update_franchise(){
    $id=$this->input->post('id');
    $franchise_name=$this->input->post('franchise_name');
    $email=$this->input->post('email');
    $contact=$this->input->post('contact');
    $prop_name=$this->input->post('prop_name');
    $address=$this->input->post('address');
    $where=array('id'=>$id);
    $data = array('franchise_id' => $franchise_id,'franchise_name' =>$franchise_name,'email'=>$email,'contact'=>$contact,'prop_name'=>$prop_name,'address'=>$address);
    $this->m_default->update($where, $data, 'tbl_franchise');
    $this->session->set_flashdata('success','franchise Edited Successfully..!');
    redirect('franchise/list_franchise');
  }

  public function list_franchise(){
    $member_id=$this->session->userdata('member_id');
    $sql="SELECT  a.* ,b.username,b.email, b.status as loginstatus FROM  tbl_franchise a,tbl_login b WHERE a.id=b.franchise_id and b.usertype=3";
    $this->data['listmember']=$this->db->query($sql)->result_array();
    $this->data['content']='franchise/list_franchise';
    $this->data['title']='List franchise | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

  public function edit_franchise(){
    $id = $this->input->post('id');
    $sql="SELECT  a.*  FROM  tbl_franchise a WHERE  a.id=$id";
    $this->data['franchisedetails']=$this->db->query($sql)->result_array();
    $this->data['id']=$id;
    $this->data['content']='franchise/edit_franchise';
    $this->data['title']='Edit franchise | WAVE EDU COIN ';
    $this->load->view('common/template',$this->data);
  }

  public function activate() {
    $id = $this->input->post('id');
    $where = array('franchise_id' => $id);
    $data = array('status' => 0);
    $flag = $this->m_default->update($where, $data, 'tbl_login');
    if($flag == true) {
      $this->session->set_flashdata('success', 'franchise Activated Successfully..!');
      redirect('franchise/list_franchise');
    }
  }

  public function deactivate() {
    $id = $this->input->post('id');
    $where = array('franchise_id' => $id);
    $data = array('status' => 1);
    $flag = $this->m_default->update($where, $data, 'tbl_login');
    if($flag == true) {
      $this->session->set_flashdata('success', 'franchise Deactivated Successfully..!');
      redirect('franchise/list_franchise');
    }
  }
}

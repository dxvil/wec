<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Package extends CI_Controller {

  /**
   * Package Controller
   * Author: Dipanwita Chanda
   **/

  public function __construct()
  {
       parent::__construct();
       $this->load->model('m_default');
       
       $sql="select coin_price from tbl_coinprice where id=1";
      $this->data['wave_coin_price'] = $this->db->query($sql)->row()->coin_price;
      // foreach($this->input->post() as $items){
      //   if ($items != ''){
      //       if (! preg_match("/^[a-z0-9A-Z@ .-]+$/i", $items)){
      //         $this->session->set_flashdata('error_login','Something went wrong');
      //         redirect($_SERVER['HTTP_REFERER']);
      //         break;
      //       }
      //   }
      // }
  }


 public function total_members_down_array($userid,$array)
  {
    $sql="SELECT  a.member_id, a.username FROM tbl_member b, tbl_login a where a.member_id=b.id and b.parent_id=$userid ";
    $records=$this->db->query($sql)->result_array();
    $array=array_merge($array,$records);
    
    foreach($records as $row)
      {
        $array= $this->total_members_down_array($row['member_id'],$array);
      }
    
      return $array;
  }
  public function index()
  {
      $member_id=$this->session->userdata('member_id');
      $this->data['memberlist']=$this->db->query("select member_id,username from tbl_login where id=$member_id")->result_array();
    
      //$this->data['memberlist']= $this->total_members_down_array($member_id,$this->data['memberlist']);
    
    $query = "SELECT package_price FROM  tbl_member where id=".$member_id;
    $this->data['package_price']= $this->db->query($query)->row()->package_price;

    
    $sql="select amount as wallet from tbl_wallet where member_id=$member_id";
    $this->data['wallet'] = $this->m_default->get_single_row($sql);
    
    $sql="select amount as fundwallet from tbl_fundwallet where member_id=$member_id";
    $this->data['fundwallet'] = $this->m_default->get_single_row($sql);
    
    $sql="SELECT id,pin FROM tbl_giftcard WHERE used=0 and (created_by=$member_id or sent_to=$member_id)";
    $this->data['epin']=$this->db->query($sql)->result_array();
    $this->data['content']='buy_package';
    $this->data['title']='Home | WAVE EDU COIN';
    $this->load->view('common/template',$this->data);
  }

public function get_member_name(){
  
      $member_id=$this->input->post('member_id'); 

      if (! preg_match("/^[a-z0-9A-Z]+$/i", $member_id)){
        echo 'member not found';
        exit();
      }


      $member_id= $this->db->query('select member_id from tbl_login where username="'.$member_id.'"')->row()->member_id; 
      echo $this->db->query('select name from tbl_member where id='.$member_id)->row()->name; 
}


public function get_current_package(){
      $member_id=$this->input->post('member_id'); 
      $member_id= $this->db->query('select member_id from tbl_login where username="'.$member_id.'"')->row()->member_id; 
      $this->data['package_price']= $this->db->query('select package_price from tbl_member where id='.$member_id)->row()->package_price; 
     
    echo $this->data['package_price'];
}

public function super_activate()
{ 

 

    $sql="select coin_price from tbl_coinprice where id=1";
    $wave_coin_price = $this->db->query($sql)->row()->coin_price;
     
    $walletmember_id = $this->input->post('member_id');
    //$pin_id=$this->input->post('pin_id');
    $bonustype='BV';
    
    $package_id=$this->input->post('package_id');

     if (! preg_match("/^[0-9]+$/i", $package_id)){
       $this->session->set_flashdata('danger','Something went wrong');
       redirect('home/super_package');
     }
     

   if($package_id==20){
       $package_price=100;
   }

  else if($package_id==21){
    $package_price=500;
  }

  else if($package_id==22){
    $package_price=1000;
  }

  else if($package_id==23){
    $package_price=2000;
  }

  else if($package_id==24){
    $package_price=5000;
  }

  else if($package_id==25){
    $package_price=10000;
  }
   
   
   
    $price=$package_price;
        
 
       
    if(1+2 == 2){
        $this->session->set_flashdata('danger','Insufficient Balance..!');
        redirect('home/super_package');
    }
    else{
        
    $member_id=$this->input->post('member_id');

    if (! preg_match("/^[0-9]+$/i", $member_id)){
       $this->session->set_flashdata('danger','Something went wrong');
       redirect('home/super_package');
     }
    
    
    if(!isset($member_id)){
          $this->session->set_flashdata('danger','Invalid Member Id..!');
          redirect('home/super_package');
    }
    $child_id=$member_id;
    $query = "SELECT parent_id, member_status,sponser_id FROM  tbl_member  WHERE id=$member_id";
    $num_rows = $this->m_default->get_single_row($query);
    
    
    $query_din = "SELECT username FROM  tbl_login WHERE member_id=$walletmember_id";
    $num_rows_din = $this->m_default->get_single_row($query_din);

    

    $rWallet = 0;
    $coins = 0;
    
    $array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
    $this->m_default->data_insert('tbl_member_package',$array);
    
    
    $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
    
    $pv=$package_price;
            
    //$this->savedetails($num_rows[0]['sponser_id'],$member_id,(($package_price*10)/100),0,'Direct','Direct Income','');

            
    $date= date('Y-m-d h:i:s');
    $recharge_username = 'admin';
    $ryi = 2;
    $query="update tbl_member set member_status=1 ,package_add_date='$date',package_id=$package_id,package_price=package_price+$package_price,pv=pv+$pv, recharge_id='$recharge_username',payment_type=$ryi where id=$member_id";
    $this->m_default->execute_query($query);
    $parent_id=$member_id;
     

    $sql="SELECT parent_id,leg FROM tbl_member  where id=".$member_id;
    $nextstep=$this->db->query($sql)->result_array();
              
    if($nextstep[0]['parent_id']!=""){
            $this->recursive_leg_sponser($nextstep[0]['parent_id'],$nextstep[0]['leg'],$pv);
    }


    // $useddate=date('Y-m-d');
    /*$query="update tbl_giftcard set used=1,used_by=$member_id,used_date='".$useddate."' where id=$pin_id";
    $this->m_default->execute_query($query);*/

    
   
  $this->session->set_flashdata('success','Activated Successfully');
   
    //}
    
    
     }
     redirect('home/super_package');
}



 public function activate()
 {
     
     $sql="select coin_price from tbl_coinprice where id=1";
     $wave_coin_price = $this->db->query($sql)->row()->coin_price;
      
     $walletmember_id=$this->session->userdata('member_id');
     $pin_id=$this->input->post('pin_id');
     $bonustype='BV';
     
     $package_id=$this->input->post('package_id');

      if (! preg_match("/^[0-9]+$/i", $package_id)){
        $this->session->set_flashdata('danger','Something went wrong');
        redirect($_SERVER['HTTP_REFERER']);
      }
      
    if ($package_id == 25){
      $package_price=25;
    }  
    else if($package_id==1 || $package_id==2){
        $package_price=100;
    }
    else if($package_id==3 || $package_id==4){
        $package_price=200;
    }
    else if($package_id==5 || $package_id==6){
        $package_price=300;
    }
    else if($package_id==7 || $package_id==8){
        $package_price=500;
    }
    else if($package_id==9){
        $package_price=1000;
    }
    else if($package_id==10){
        $package_price=2000;
    }
    else if($package_id==11){
        $package_price=3000;
    }
    else if($package_id==12){
        $package_price=5000;
    }
    else if($package_id==13){
        $package_price=10000;
    }
    else if($package_id==14){
        $package_price=100;
    }
    else if($package_id==15){
        $package_price=200;
    }
    else if($package_id==16){
      $package_price=100;
    }
    else if($package_id==17){
        $package_price=200;
    }
    else if($package_id==18){
      $package_price=300;
    }
    else if($package_id==19){
      $package_price=500;
    }
    else if($package_id==20){
      $package_price=1000;
    }
    else if($package_id==21){
      $package_price=2000;
    }
    else if($package_id==22){
      $package_price=3000;
    }
    else if($package_id==23){
      $package_price=5000;
    }
    else if($package_id==24){
      $package_price=10000;
    }
    
    
     $price=$package_price;
     
     $payment=$this->input->post('package_method');
     
    //  if($payment=="e_wallet"){
      $amountepin=$this->db->query("select amount from tbl_wallet where member_id=$walletmember_id")->row()->amount;   
    //  }
     
    //  if($payment=="fund_wallet"){
      $fundamountepin=$this->db->query("select amount from tbl_fundwallet where member_id=$walletmember_id")->row()->amount;   
    //  }
      /*
      if ($payment == "" || !($payment >= 1 && $payment <= 3)){

        $this->session->set_flashdata('danger','Payment method invalid');
        redirect('package');

      }

      if ($payment == 1 && ($fundamountepin < $price)){

        $this->session->set_flashdata('danger','Insufficient Balance..!');
        redirect('package');

      }

      else if ($payment == 2 && (($fundamountepin < $price / 2) || ($amountepin < $price / 2))){

        $this->session->set_flashdata('danger','Insufficient Balance..!');
        redirect('package');

      }

      else if ($payment == 3 && (((int) $amountepin < 100 && ($amountepin+$fundamountepin)<$price))){

        $this->session->set_flashdata('danger','Insufficient Balance..!');
        redirect('package');

      }
      */

      if(($amountepin+$fundamountepin)<$price){
        $this->session->set_flashdata('danger','Insufficient Balance..!');
        redirect('package');
      }

     else{

      if ($package_id == 25 && $payment == 1 && ($fundamountepin < $price)){

        if ($fundamountepin < $price){
          $this->session->set_flashdata('danger','Insufficient Balance..!');
          redirect('package');
        }

        else {
          $this->session->set_flashdata('danger', 'This package only support fund wallet..!');
          redirect('package');
        }

      }

      if ($package_id == 25 && $payment != 1){
          $this->session->set_flashdata('danger', 'This package only support fund wallet..!');
          redirect('package');
      }
         
     $member_idusername=$this->input->post('member_id');

     if (! preg_match("/^[a-z0-9A-Z]+$/i", $member_idusername)){
        $this->session->set_flashdata('danger','Something went wrong');
        redirect($_SERVER['HTTP_REFERER']);
      }
     
     $member_id=$this->db->query('select member_id from tbl_login where username="'.$member_idusername.'"')->row()->member_id;
     if(!isset($member_id)){
           $this->session->set_flashdata('danger','Invalid Member Id..!');
         redirect('package');
     }
     
     $child_id=$member_id;
     $query = "SELECT parent_id, member_status,sponser_id FROM  tbl_member  WHERE id=$member_id";
     $num_rows = $this->m_default->get_single_row($query);
     
     
     $query_din = "SELECT username FROM  tbl_login  WHERE member_id=$walletmember_id";
     $num_rows_din = $this->m_default->get_single_row($query_din);



    //  if($num_rows[0]['member_status']==1)
    //  {
    //  $this->session->set_flashdata('danger','Already Activated..!');
    //  redirect('package');
    //  }
    //  else
    //  {
         
        if($package_id==1){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==2){ $coins=((($package_price))/$wave_coin_price)*70; } 
        elseif($package_id==3){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==4){ $coins=((($package_price))/$wave_coin_price)*70; } 
        elseif($package_id==5){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==6){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==7){ $coins=((($package_price*10)/100)/$wave_coin_price)*70;}
        elseif($package_id==8){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==9){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==10){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==11){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==12){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==13){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==14){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==15){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id >= 16 && $package_id <= 24){ 
          
          
          $coins=((($package_price))/$wave_coin_price)*70;
          $relase_dollar = (($package_price*10)/100);
          $bonus_dollar = (($relase_dollar*2)/100);


          $r = $this->db->query("select id from tbl_bonus_wallet where user_id = $member_id");

          if (count($r->row()) > 0){

            $this->db->query("update tbl_bonus_wallet set amount=amount+$bonus_dollar where user_id=$member_id");


          }

          else {

            $array=array('user_id'=>$member_id,'amount'=>$bonus_dollar);
            $this->m_default->data_insert('tbl_bonus_wallet',$array);

          }


          $array=array('member_id'=>$member_id,'amount'=>$bonus_dollar,'percent'=>2, 'created_on' => date('Y-m-d h:i:s'));
          $this->m_default->data_insert('tbl_bonus_report',$array);


        }
        else{

          $coins=0;
          
        }

        
        // $array=array('member_id'=>$member_id,'coins'=>$coins);
        // $this->m_default->data_insert('tbl_coin',$array);
        
        $this->db->query("update tbl_coin set coins=coins+$coins, coin_price = $wave_coin_price where member_id=$member_id");
    
        if($package_id==1){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==2 || $package_id == 16){ $coins=((($package_price))/$wave_coin_price)*70; } 
        elseif($package_id==3){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==4 || $package_id == 17){ $coins=((($package_price))/$wave_coin_price)*70; } 
        elseif($package_id==5){ $coins=((($package_price*10)/100)/$wave_coin_price)*70; } 
        elseif($package_id==6 || $package_id == 18){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==7){ $coins=((($package_price*10)/100)/$wave_coin_price)*70;}
        elseif($package_id==8 || $package_id == 19){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==9 || $package_id == 20){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==10 || $package_id == 21){ $coins=((($package_price))/$wave_coin_price)*70; }
        elseif($package_id==11 || $package_id == 22){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==12 || $package_id == 23){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==13 || $package_id == 24){ $coins=((($package_price))/$wave_coin_price)*70;  }
        elseif($package_id==14){ $coins=(($package_price)/$wave_coin_price)*70;  }
        elseif($package_id==15){ $coins=(($package_price)/$wave_coin_price)*70;  }
        $rWallet= $package_id == 25 ? 0 : ($coins*10)/100;
        
        // $array=array('member_id'=>$member_id,'coins'=>$rWallet);
        // $this->m_default->data_insert('tbl_release_wallet',$array);
        
        if($package_id==1 || $package_id==3 || $package_id==5 || $package_id==7){
            $rWallet=$coins;
        }
        
        if($package_id==14){
           $rWallet= ($coins*40)/100;
        }
        
        if($package_id==15){
           $rWallet= ($coins*50)/100;
        }
        
        
        $array=array('member_id'=>$member_id,'package_id'=>$package_id,'package_price'=>$package_price,'coin'=>$coins,'r_coin'=>$rWallet);
        $this->m_default->data_insert('tbl_member_package',$array);
        
        
        $this->db->query("update tbl_release_wallet set coins=coins+$rWallet where member_id=$member_id");
        
        $pv=$package_price;

        $income = (($package_price*10)/100);

        if ($package_id==25){

          $sql="select sum(amount) as direct from tbl_bonus where member_id=$member_id and bonus_type='Direct'";
          $direct = $this->data['direct'] = $this->m_default->get_single_row($sql);
          $direct = empty($direct[0]['direct'])?0.00:round($direct[0]['direct'],2);

          $sql="select sum(amount) as mbv from tbl_bonus where bonus_type='MBV' and member_id=".$member_id;
          $mbv = $this->m_default->get_single_row($sql);
          $mbv = empty($mbv[0]['mbv'])?0.00:round($mbv[0]['mbv'],2);

          $to = ($direct + $mbv);

          if ($to >= 245){
            $income = 0;
          }

        }

        
                
        $this->savedetails($num_rows[0]['sponser_id'],$member_id,$income,0,'Direct','Direct Income','');

                
     $date= date('Y-m-d h:i:s');
     $recharge_username = $num_rows_din[0]['username'];
     $recharge_type = $this->input->post('package_method');
     $ryi = $recharge_type;
     $query="update tbl_member set member_status=1 ,package_add_date='$date',package_id=$package_id,package_price=package_price+$package_price,pv=pv+$pv, recharge_id='$recharge_username',payment_type=$ryi where id=$member_id";
     $this->m_default->execute_query($query);
     $parent_id=$member_id;
      

     $sql="SELECT parent_id,leg FROM tbl_member  where id=".$member_id;
     $nextstep=$this->db->query($sql)->result_array();
               
     if($nextstep[0]['parent_id']!=""){
             $this->recursive_leg_sponser($nextstep[0]['parent_id'],$nextstep[0]['leg'],$pv);
     }


     // $useddate=date('Y-m-d');
     /*$query="update tbl_giftcard set used=1,used_by=$member_id,used_date='".$useddate."' where id=$pin_id";
     $this->m_default->execute_query($query);*/


/*
if($payment==3){
   
        $restamount= abs($price-$amountepin);


        
        $this->db->query("update tbl_wallet set amount=amount-$amountepin where member_id=$walletmember_id"); 
        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
        $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$amountepin,'balance'=>$debitedamount,'status'=>1);
        $this->m_default->data_insert('tbl_wallet_report',$array);
        
        $amountepin=$this->db->query("update tbl_fundwallet set amount=amount-$restamount where member_id=$walletmember_id");    
        $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
        $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$restamount,'balance'=>$debitedamount,'status'=>1);
        $this->m_default->data_insert('tbl_fundwallet_report',$array);
      

     }


    else if($payment==2){
   
      
        $restamount=$price / 2;

        $amountepin = $price / 2;

        $this->db->query("update tbl_wallet set amount=amount-$amountepin where member_id=$walletmember_id"); 
        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
        $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$amountepin,'balance'=>$debitedamount,'status'=>1);
        $this->m_default->data_insert('tbl_wallet_report',$array);

        $amountepin=$this->db->query("update tbl_fundwallet set amount=amount-$restamount where member_id=$walletmember_id");    
        $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
        $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$restamount,'balance'=>$debitedamount,'status'=>1);
        $this->m_default->data_insert('tbl_fundwallet_report',$array);
      
        
    }
     
    else if($payment==1){
       
      $this->db->query("update tbl_fundwallet set amount=amount-$price where member_id=$walletmember_id");    
        $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
        $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
        $this->m_default->data_insert('tbl_fundwallet_report',$array);
       
     }
     
     */

    if($payment==0){
   
   
      
    
      if($amountepin<$price){
          $restamount=$price-$amountepin;
          
          $this->db->query("update tbl_wallet set amount=amount-$amountepin where member_id=$walletmember_id"); 
        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
     $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$amountepin,'balance'=>$debitedamount,'status'=>1);
     $this->m_default->data_insert('tbl_wallet_report',$array);
     
     $amountepin=$this->db->query("update tbl_fundwallet set amount=amount-$restamount where member_id=$walletmember_id");    
     $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
     $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$restamount,'balance'=>$debitedamount,'status'=>1);
     $this->m_default->data_insert('tbl_fundwallet_report',$array);
     
      }
      else{
          $this->db->query("update tbl_wallet set amount=amount-$price where member_id=$walletmember_id"); 
        $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
     $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
     $this->m_default->data_insert('tbl_wallet_report',$array);
      }
      
        
       }
       
      else if($payment==1){
          
          if($fundamountepin<$price){
          $restamount=$price-$fundamountepin;
          
          $this->db->query("update tbl_fundwallet set amount=amount-$fundamountepin where member_id=$walletmember_id");    
        
         $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
      $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$fundamountepin,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_fundwallet_report',$array);
     
      $this->db->query("update tbl_wallet set amount=amount-$restamount where member_id=$walletmember_id"); 
      $debitedamount=$this->db->query("Select amount from tbl_wallet where member_id=$walletmember_id ")->row()->amount;
      $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$restamount,'balance'=>$debitedamount,'status'=>1);
      $this->m_default->data_insert('tbl_wallet_report',$array);
     
      }
      else{
     $this->db->query("update tbl_fundwallet set amount=amount-$price where member_id=$walletmember_id");    
     $debitedamount=$this->db->query("Select amount from tbl_fundwallet where member_id=$walletmember_id ")->row()->amount;
     $array=array('member_id'=>$walletmember_id,'desc'=>'Member Id Activated','debited'=>$price,'balance'=>$debitedamount,'status'=>1);
     $this->m_default->data_insert('tbl_fundwallet_report',$array);
      }
       }
    
     $this->session->set_flashdata('success','Activated Successfully');
    
     //}
     
     
      }
      redirect('package');
 }


 public function recursive_leg_sponser($userid,$leg,$pv)
  {
     $sql="SELECT parent_id,leg FROM tbl_member  where id=$userid"; 
    $records=$this->db->query($sql)->result_array();
    
      if($leg==0){
        $this->db->query("update tbl_member set totalleftpv=totalleftpv+$pv where id=".$userid);
    }
    
     if($leg==1){
        $this->db->query("update tbl_member set totalrightpv=totalrightpv+$pv where id=".$userid);
    }
    
    
    $leg=$records[0]['leg'];
  
      foreach($records as $row)
        {
          return $this->recursive_leg_sponser($row['parent_id'],$leg,$pv);
      }

  }

 public function savedetails($parent_id,$member_id,$amount,$bv_points,$bonustype,$level)
 {
   $array=array('member_id'=>$parent_id,'child_id'=>$member_id,'amount'=>$amount,'bv_amount'=>$bv_points,'bonus_type'=>$bonustype,'level'=>$level);
   $this->m_default->data_insert('tbl_bonus',$array);

   $query="update tbl_wallet set amount=amount+$amount where member_id=$parent_id";
   $this->m_default->execute_query($query);
 }

public function treeview()
{
  $member_id=$this->session->userdata('member_id');
  $str='';
  $str1=$this->m_treeview($member_id,$str);
  $this->data['str']=$str1;
  $this->data['content']='tree_view';
  $this->data['title']='Tree View | WAVE EDU COIN';
  $this->load->view('common/template',$this->data);
}

public function m_treeview($userid,$str)
{
  $sql="SELECT * FROM tbl_member where parent_id=$userid";
  $records=$this->db->query($sql)->result_array();
  if(count($records)>0)
  {
   $str.='<ul>';
  foreach($records as $row)
    {
      $str.='<li><a href="#">'.$row['name'].'</a>';
      $str= $this->m_treeview($row['id'],$str);
      $str.='</li>';
    }
  $str.='</ul>';
  }

    return $str;

}


public function generatematching()
{
  $query = "SELECT id FROM  tbl_member where id!=1";
  $num_rows = $this->db->query($query)->result_array();
  $pairs='';
  foreach ($num_rows as $key => $value) {
    $parent_id=$num_rows[$key]['id'];

    $query = "SELECT id,name FROM  tbl_member where parent_id=".$num_rows[$key]['id']." order by leg asc";
    $count= $this->db->query($query)->result_array();

    $query = "SELECT id FROM  tbl_bonus where bonus_type='MBV' and member_id=".$num_rows[$key]['id'];
    $imbv= $this->db->query($query)->result_array();

    $countpairs=0;
 //echo 'Total Down';
  $this->total_members_down($parent_id,$countpairs);
 //left
 $query = "SELECT id,name FROM  tbl_member where leg =0 and parent_id=".$num_rows[$key]['id'];
 $leftid= $this->db->query($query)->result_array();
 if(count($leftid)>0)
 {
  $countpairs=1;
  //echo 'Left Leg';
   $totalleftleg=$this->total_members_down($leftid[0]['id'],$countpairs);
   $leftmemberids=$this->total_members_down_ids($leftid[0]['id'],$leftid[0]['id']);
 }


/*team develop*/

$leftmembersidsarray=explode(',',$leftmemberids);
$teamleft_ids= $this->db->query("SELECT left_ids FROM  tbl_team_development where member_id=".$num_rows[$key]['id'])->row()->left_ids;

$teamleft_idsarray=explode(',',$teamleft_ids);
$leftcount=0;
foreach($leftmembersidsarray as $row)
{
    $flag=0;
    foreach ($teamleft_idsarray as $row1) {
        if($row==$row1)
        {
            $flag=1;
        }
    }
    if($flag==0)
    {
      $leftcount++;
      $teamleft_ids.=$row.',';
    }

}


 //right
 $query = "SELECT id,name FROM  tbl_member where leg =1 and parent_id=".$num_rows[$key]['id'];
 $rightid= $this->db->query($query)->result_array();

 if(count($rightid)>0)
 {
  $countpairs=1;
  //echo 'Right Leg';
  $totalrightleg= $this->total_members_down($rightid[0]['id'],$countpairs);
  $rightmemberids=$this->total_members_down_ids($rightid[0]['id'],$rightid[0]['id']);
 }



 /*team develop*/


 $rightmembersidsarray=explode(',',$rightmemberids);
 $teamright_ids= $this->db->query("SELECT right_ids FROM  tbl_team_development where member_id=".$num_rows[$key]['id'])->row()->right_ids;

 $teamright_idsarray=explode(',',$teamright_ids);
 $rightcount=0;
 foreach($rightmembersidsarray as $row)
 {
     $flag=0;
     foreach ($teamright_idsarray as $row1) {
         if($row==$row1)
         {
             $flag=1;
         }
     }
     if($flag==0)
     {
       $rightcount++;
       $teamright_ids.=$row.',';
     }

 }

 if(($rightcount+$leftcount)>=75)
 {
   if($rightcount>=5 && $leftcount>=5)
   {
    $right15per=  (($rightcount*2000)*15)/100;
    $left15per=  (($leftcount*2000)*15)/100;
    $totaltdb=$right15per+$left15per;
    $this->savedetails($num_rows[$key]['id'],0,0,$totaltdb,'Team Development','N/A');

    $this->db->query("update  tbl_team_development set left_ids='$teamleft_ids',right_ids='$teamright_ids'  where member_id=".$num_rows[$key]['id']);

    $array=array('member_id'=>$parent_id,'left_count'=>$leftcount,'right_count'=>$rightcount,'left_percentage'=>$left15per,'right_percentage'=>$right15per,'total_bv'=>$totaltdb);
    $this->m_default->data_insert('tbl_development_bonus',$array);

  /*echo $teamleft_ids;
    echo $leftcount;
    echo '<br>';
    echo $teamright_ids;
    echo $rightcount;*/

   }

/*team royalty*/
   if($rightcount>=10 && $leftcount>=10)
   {
    $right15per=  (($rightcount*2000)*15)/100;
    $left15per=  (($leftcount*2000)*15)/100;
    $totaltdb=$right15per+$left15per;
    $this->savedetails($num_rows[$key]['id'],0,0,$totaltdb,'Team Development','N/A');

    $this->db->query("update  tbl_team_development set left_ids='$teamleft_ids',right_ids='$teamright_ids'  where member_id=".$num_rows[$key]['id']);

    $array=array('member_id'=>$parent_id,'left_count'=>$leftcount,'right_count'=>$rightcount,'left_percentage'=>$left15per,'right_percentage'=>$right15per,'total_bv'=>$totaltdb);
    $this->m_default->data_insert('tbl_development_bonus',$array);

  /*echo $teamleft_ids;
    echo $leftcount;
    echo '<br>';
    echo $teamright_ids;
    echo $rightcount;*/

   }
 }

   if(count($imbv)>0)
   {
 $query = "SELECT left_used,right_used FROM  tbl_pairsdata where member_id= ".$num_rows[$key]['id'];
 $leftdata= $this->db->query($query)->result_array();
$newleft=$totalleftleg-$leftdata[0]['left_used'];
$newright=$totalrightleg-$leftdata[0]['right_used'];
if($newleft!=0 && $newright!=0)
{
  if($newleft==$newright)
  {
    $newpairs=$newleft;
    $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>0,'left_carry'=>0,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
    $this->m_default->data_insert('tbl_matching_amount',$array);
  }
  else if($newright>$newleft)
  {
    $newpairs=$newleft;
    $carryright=$newright-$newleft;
    $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>$carryright,'left_carry'=>0,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
    $this->m_default->data_insert('tbl_matching_amount',$array);
  }
  else if($newleft>$newright)
  {
    $newpairs=$newright;
    $carryleft=$newleft-$newright;
    $array=array('member_id'=>$parent_id,'amount'=>(400*$newpairs),'right_carry'=>0,'left_carry'=>$carryleft,'left'=>$newleft,'right'=>$newright,'pairs'=>$newpairs,'mbv_type'=>'1:1');
    $this->m_default->data_insert('tbl_matching_amount',$array);
  }
  $query = "update tbl_pairsdata set left_used = left_used+$newpairs,right_used =right_used+$newpairs,total_pairs=total_pairs+$newpairs  where member_id =".$num_rows[$key]['id'];
  $this->db->query($query);
for($i=0;$i<$newpairs;$i++){
  $this->savedetails($parent_id,0,400,4000,'MBV','N/A');
}
}
   }
   else{
 if(count($count)==2)
    {
      $pairs=$count[0]['id'].','.$count[1]['id'];
      $query = "SELECT id,name FROM  tbl_member where parent_id=".$count[0]['id'];
      $child1= $this->db->query($query)->result_array();

      $query = "SELECT id,name FROM  tbl_member where parent_id=".$count[1]['id'];
      $child2= $this->db->query($query)->result_array();

      if(count($child1)>0)
      {
      $pairs.=','.$child1[0]['id'];
      $this->savedetails($parent_id,0,400,6000,'MBV','N/A');
      $array=array('member_id'=>$parent_id,'pair_ids'=>$pairs,'amount'=>400,'right_carry'=>($totalrightleg-1),'left_carry'=>($totalleftleg-2),'left'=>$totalleftleg,'right'=>$totalrightleg,'pairs'=>1,'mbv_type'=>'2:1');
      $this->m_default->data_insert('tbl_matching_amount',$array);

      $array=array('member_id'=>$parent_id,'left_used'=>2,'right_used'=>1,'total_pairs'=>1);
      $this->m_default->data_insert('tbl_pairsdata',$array);
      }
      else if(count($child2)>0)
      {
        $pairs.=','.$child2[0]['id'];
        $this->savedetails($parent_id,0,400,4000,'MBV','N/A');
        $array=array('member_id'=>$parent_id,'pair_ids'=>$pairs,'amount'=>400,'right_carry'=>($totalrightleg-2),'left_carry'=>($totalleftleg-1),'left'=>$totalleftleg,'right'=>$totalrightleg,'pairs'=>1,'mbv_type'=>'1:2');
        $this->m_default->data_insert('tbl_matching_amount',$array);

        $array=array('member_id'=>$parent_id,'left_used'=>1,'right_used'=>2,'total_pairs'=>1);
        $this->m_default->data_insert('tbl_pairsdata',$array);
      }
      else {
        continue;
      }
    }
    else {
    continue;
    }
$pairs='';}
}
   $this->session->set_flashdata('success','Payout Generated Successfully..!');
   redirect('pay_out/list_payout');
}


public function total_members_down($userid,$count)
{
  $sql="SELECT * FROM tbl_member where parent_id=$userid ";
  $records=$this->db->query($sql)->result_array();
  $count+=count($records);
  if($count>0)
  {
  foreach($records as $row)
    {
      $count= $this->total_members_down($row['id'],$count);
    }
  }
    return $count;
}

public function total_members_down_ids($userid,$ids)
{
  $sql="SELECT * FROM tbl_member where parent_id=$userid ";
  $records=$this->db->query($sql)->result_array();
  $count=count($records);

  if($count>0)
  {
  foreach($records as $row)
    {
      $ids.=','.$row['id'];
      $ids= $this->total_members_down_ids($row['id'],$ids);
    }
  }
    return $ids;
}

// public function total_members_down($userid,$wallet)
// {
//   $sql="SELECT a.*, FROM tbl_member where parent_id=$userid ";
//   $records=$this->db->query($sql)->result_array();
//   $wallet+=count($records);
//   if($count>0)
//   {
//   foreach($records as $row)
//     {
//       $wallet= $this->total_members_down($row['id'],$wallet);
//     }
//   }
//     return $wallet;
// }

}

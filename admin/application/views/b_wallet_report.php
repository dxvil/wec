<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- [ breadcrumb ] start -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Wallet Report</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Wallet Report</li>
            </ol>
          </div>
        </div>
        <!-- ============================ -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================-->
        <!-- ============================-->
        <!-- Container fluid  -->
        <!-- ============================-->
        <div class="container-fluid">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h5>List Wallet Report</h5>
              </div>
              <div class="card-block">
                <div class="table-responsive">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Percentage</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1;
                      $approve_url=base_url()."index.php/home/w_activate";
                      foreach ($wallet_report as $value) {?>
                      <tr>
                        <td><?php echo  $i; ?></td>
                        <td><?php echo date('d-m-Y',strtotime($value['created_on']))?></td>
                        <td><?php echo  $value['percent']?>%</td>
                        <td><?php echo  $value['amount']?></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Percentage</th>
                        <th>Amount</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        

<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">List Confirmed Withdraw</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">List Confirmed Withdraw</li>
                </ol>
              </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
              <!-- ===================== -->
              <!-- Start Page Content -->
              <!-- ===================== -->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>List Confirmed Withdraw</h5>
                  </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Withdraw Amount</th>
                            <th>Withdraw Status</th>
                            <th>Request Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; foreach ($withdraw_list as $value) {?>
                          <tr>
                            <td><?php echo  $i; ?></td>
                            <td><?php echo  $value['withdraw_amount']?></td>
                            <td><?php if($value['withdraw_status']==0)
                              echo 'Pending';
                              else{
                                echo 'Approved';
                              }
                            ?></td>
                            <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>#</th>
                            <th>Withdraw Amount</th>
                            <th>Withdraw Status</th>
                            <th>Request Date</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

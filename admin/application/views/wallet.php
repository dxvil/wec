<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- [ breadcrumb ] start -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">My Wallet</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Wallet</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Wallet </h4>
                  <form class="form p-t-20">
                    <div class="form-group">
                      <label for="exampleInputuname">Wallet Balance</label>
                      <!--<div class="input-group">-->
                      <!--  <div class="input-group-addon">-->
                      <!--    <i class="fa fa-credit-card"></i>-->
                      <!--  </div>-->
                        <input type="text" class="form-control" id="exampleInputuname" placeholder="" value="$<?php echo $totalbonus[0]['totalbonus'];?>" readonly>
                      <!--</div>-->
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">List News</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">List News</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->

                                <?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                               <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-primary" role="alert">
                                        <p>check out the latest news</p>
										<!--<label class="text-muted">Copy/paste source code in your page in just couples of seconds.</label>-->
                                    </div>
                                </div>
                                
                                <?php
$i=0; foreach ($list_news as $key => $value) {

?>
                                <!-- [ dashboard-ui-1 ] start -->
                                <div class="col-xl-4 col-md-6">
                                    <div class="card">
                                        <img class="img-fluid" src="<?php echo base_url()?>uploads/newsimage/<?php echo $value['image'];?>" alt="dashboard-user">
                                        <div class="card-block">
                                            <h5><?php echo $value['subject']; ?></h5>
                                            <span class="text-muted"><?php echo substr($value['message'],0,100); ?></span>
                                            <div class="row m-t-30">
                                                <div class="col-6 p-r-0">
                                                    <a href="<?php echo base_url()?>index.php/news/view_news1/<?php echo $value['id']?>" class="btn btn-primary text-uppercase btn-block">Read More</a>
                                                </div>
                                                <div class="col-6">
                                                    <a href="#!" class="btn text-uppercase border btn-block btn-outline-secondary">message</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ dashboard-ui-1 ] end -->
<?php } ?>

                              
                            </div>

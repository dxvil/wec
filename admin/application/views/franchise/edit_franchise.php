<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Edit Franchise</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Edit Franchise</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
          <?php

$error=$this->session->flashdata('error_login');
echo (!empty($error))?
"<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                        <div class="card">
                            <div class="card-body">

                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/franchise/update_franchise" method="post" enctype="multipart/form-data" accept-charset="utf-8">

                                  <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Franchise ID</label>
                                            <input type="text" class="form-control"  id="franchise_id" name="franchise_id" placeholder="Franchise ID" value="<?php echo $franchisedetails[0]['franchise_id']?>" required readonly>

                                            <input type="hidden" class="form-control"  id="id" name="id"  value="<?php echo $franchisedetails[0]['id']?>" >
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Franchise Name
</label>
                                            <input type="text" class="form-control" id="franchise_name" name="franchise_name" placeholder="Franchise Name" value="<?php echo $franchisedetails[0]['franchise_name']?>" required>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Contact Number
                                  </label>
                                            <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number" value="<?php echo $franchisedetails[0]['contact']?>" required>

                                    </div>
                                  </div>
                                  <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="exampleInputuname">Email

                                    </label>
                                    <input type="text" class="form-control" id="email"  name="email" placeholder="Email" value="<?php echo $franchisedetails[0]['email']?>">

                                    </div>

                                  <div class="form-group">
                                      <label for="exampleInputuname">Prop Name

                                  </label>
                                  <input type="text" class="form-control" id="prop_name"  name="prop_name" placeholder="Prop Name" value="<?php echo $franchisedetails[0]['prop_name']?>">

                                  </div>

                                  <div class="form-group">
                                    <label for="exampleInputuname">Address</label>
                                        <textarea type="file" class="form-control" id="address"  name="address" placeholder="Address" ><?php echo $franchisedetails[0]['address']?></textarea>
                                  </div>
                                  </div>

                                  <div class="col-md-4">

                                  </div>
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Add</button>
  <button type="reset"  class="btn btn-inverse" >Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <script type="text/javascript">

            $('#submit').click(function(){

                                   if ($('#franchise_id').val()=='') {
                                     $('#franchise_id').focus();
                                     return false;
                                   }

                     if ($('#name').val()=='') {
                       $('#name').focus();
                       return false;
                     }
                     if ($('#contact_no').val()=='') {
                       $('#contact_no').focus();
                       return false;
                     }

                     if ($('#email').val()=='') {
                       $('#email').focus();
                       return false;
                     }

                     if ($('#password').val()=='') {
                       $('#password').focus();
                       return false;
                     }

                      if ($('#confirm_password').val()=='') {
                       $('#confirm_password').focus();
                       return false;
                     }

             if ($('#password').val().length <6) {


                      alert('Please Enter a password more than 6 character.');
                      return false;
                    }
            if ($('#password').val()!== $('#confirm_password').val()) {


                     alert('Password Mismatch.');
                      return false;
                    }

            });
            </script>

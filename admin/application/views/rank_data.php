

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">Rank Data</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Rank Data</li>
                                    </ol>
                                </div>

                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">
                     
                                          <h4 class="card-title">List Rank Data</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                      <tr>
                                                          <th>#</th>
                                                          <th>Rank </th>
                                                          <th>Left</th>
                                                          <th>Right</th>
                                                          <th>Total</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>
                                                          <th>Rank </th>
                                                          <th>Left</th>
                                                          <th>Right</th>
                                                          <th>Total</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        
                                                      <tr>
                                                         <td>1</td>
                                                          <td>Distributor</td>
                                                          <td><?php echo  $distributorleft;?></td>
                                                          <td><?php echo  $distributorright;?></td>
                                                          <td><?php echo  ($distributorleft+$distributorright);?></td>

                                                      </tr>
                                                      
                                                         
                                                      <tr>
                                                         <td>2</td>
                                                          <td>Bronze</td>
                                                          <td><?php echo  $bronzeleft;?></td>
                                                          <td><?php echo  $bronzeright;?></td>
                                                          <td><?php echo  ($bronzeleft+$bronzeright);?></td>

                                                      </tr>
                                                      
                                                      <tr>
                                                         <td>3</td>
                                                          <td>Silver</td>
                                                          <td><?php echo  $silverleft;?></td>
                                                          <td><?php echo  $silverright;?></td>
                                                          <td><?php echo  ($silverleft+$silverright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>4</td>
                                                          <td>Platinum</td>
                                                          <td><?php echo  $platinumleft;?></td>
                                                          <td><?php echo  $platinumright;?></td>
                                                          <td><?php echo  ($platinumleft+$platinumright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>5</td>
                                                          <td>Gold </td>
                                                          <td><?php echo  $goldleft;?></td>
                                                          <td><?php echo  $goldright;?></td>
                                                          <td><?php echo  ($goldleft+$goldright);?></td>

                                                      </tr>
                                                       <tr>
                                                         <td>6</td>
                                                          <td>Pearl</td>
                                                          <td><?php echo  $pearlleft;?></td>
                                                          <td><?php echo  $pearlright;?></td>
                                                          <td><?php echo  ($pearlleft+$pearlright);?></td>

                                                      </tr>
                                                       <tr>
                                                         <td>7</td>
                                                          <td>Diamond</td>
                                                          <td><?php echo  $diamondleft;?></td>
                                                          <td><?php echo  $diamondright;?></td>
                                                          <td><?php echo  ($diamondleft+$diamondright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>8</td>
                                                          <td>Ruby</td>
                                                          <td><?php echo  $rubyleft;?></td>
                                                          <td><?php echo  $rubyright;?></td>
                                                          <td><?php echo  ($rubyleft+$rubyright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>9</td>
                                                          <td>Red Diamond</td>
                                                          <td><?php echo  $reddiamondleft;?></td>
                                                          <td><?php echo  $reddiamondright;?></td>
                                                          <td><?php echo  ($reddiamondleft+$reddiamondright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>10</td>
                                                          <td>Kohinoor</td>
                                                          <td><?php echo  $kohinoorleft;?></td>
                                                          <td><?php echo  $kohinoorright;?></td>
                                                          <td><?php echo  ($kohinoorleft+$kohinoorright);?></td>

                                                      </tr>
                                                      
                                                       <tr>
                                                         <td>11</td>
                                                          <td>Shine Bharat</td>
                                                          <td><?php echo  $shineleft;?></td>
                                                          <td><?php echo  $shineright;?></td>
                                                          <td><?php echo  ($shineleft+$shineright);?></td>

                                                      </tr>

                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->

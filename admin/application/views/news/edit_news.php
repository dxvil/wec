<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Edit News</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">List News</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Promotional Link</h4> -->
                                <!-- <h6 class="card-subtitle">Share this link </h6> -->
                                <form method="post" action="<?php echo base_url()?>index.php/news/update" accept-charset="utf-8" enctype="multipart/form-data">
                                  <div class="form-group">
                                                                          <label for="exampleInputuname">Subject</label>

                                                                              <input type="text" class="form-control" id="subject" name="subject" placeholder="" required value="<?php echo $newsdetails[0]['subject']?>">

                                                                              <input type="hidden" class="form-control" id="id" name="id" placeholder="" required value="<?php echo $newsdetails[0]['id']?>">

                                                                      </div>

            <!-- <div class="form-group">
                <label for="exampleInputuname">Image</label>

                    <input type="file" class="form-control" id="userfile" name="userfile" >

            </div> -->

              <div class="form-group">
                                                      <label for="exampleInputuname">Message</label>

                                                          <textarea id="message" name="message" class="form-control"><?php echo $newsdetails[0]['message']?></textarea>

                                                  </div>

                                                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                                  <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<script src="<?php echo base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>
    <script>
    $(document).ready(function() {

        if ($("#message").length > 0) {
            tinymce.init({
                selector: "textarea#message",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });
        }
    });
    </script>

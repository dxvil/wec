<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Referal Link</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Referal Link</li>
            </ol>
        </div>
        
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
            <form class="form-horizontal form-material" id="loginform" method="post" action="<?php echo base_url()?>index.php/team/save_member">
                        <div class="card">
                            <div class="card-body">
                    
                        <h3 class="box-title m-b-20">Sponser Information</h3>


                        <?php

                      $error=$this->session->flashdata('success');
                      echo (!empty($error))?
                      "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

                        <?php

$error=$this->session->flashdata('error_login');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>

                        <div class="form-group">
                          <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" required="" id="sponser_id" name="sponser_id"  placeholder="Sponser Id" required value="<?php echo $sponser_id;?>" readonly>
                            </div>
                             <div class="col-sm-6">
                                <input class="form-control" type="text" required="" placeholder="Sponser Name" name="sponser_name" id="sponser_name" value="<?php echo $spname;?>" readonly>
                            </div>
                          </div>
                        </div>
                        <!--<hr style="height:1px;border:none;color:#ccc;background-color:#333;" />-->
                        <!--<hr style="border: none; border-bottom: 1px solid red;">-->
                         <h3 class="box-title m-b-20">Applicant Information</h3>
                        <div class="form-group">
                          <div class="row">
                            
                            <div class="col-sm-6">
                                
                                <input class="form-control" type="text" required="" placeholder="Direction" name="direction" id="direction" value="Direction: <?php echo $direction; ?>" readonly>
 <!--                               <select name="direction" id="direction" class="form-control" required>-->
	<!--	<option selected="selected" value="" >Select Direction</option>-->
	<!--	<option value="0">Left</option> -->
	<!--	<option  value="1">Right</option>-->

	<!--</select>-->
	
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" required="" placeholder="Name" name="name" id="name" value="Demo<?php echo set_value('name'); ?>">
                            </div>

                          </div>
                        </div>
                       
                        
                        <div class="form-group">
                          <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="number" required="" id="contact_no" name="contact_no" placeholder="Contact No" value="0052555858<?php echo set_value('contact_no'); ?>">
                            </div>

                          <div class="col-sm-6">
                                <input class="form-control" type="email"  placeholder="Email" id="email" name="email" value="<?php echo set_value('email'); ?>">
                              </div>

                          </div>
                        </div>
                        
                       
                      
                            
                            <div class="form-group">
                          <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text"  id="pan_no" name="pan_no" value="<?php echo set_value('pan_no'); ?>" placeholder="Pan Number">
                            </div>

                             <div class="col-sm-6">
                              <select id="plan" name="plan" class="form-control">
                                   <option value="1">Intas Bronze(1000/-)</option>
                                   <option value="2">Intas Silver(2000/-)</option>
                                   <option value="3">Intas Gold(5000/-)</option>
                                   <option value="4">Intas Platinum(8000/-)</option>
                                   <option value="5">Intas Diamond(12000/-)</option>
                                   <option value="6">Intas Kohinoor(15000/-)</option>
                                   <option value="7">Intas High 10PV(9999/-)</option>
                                   <option value="8">Intas High 20PV(19999/-)</option>
                                   </select>
                            </div>



                         </div>
                        </div>
                        
                      
                        
                       
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-danger btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="submit">Sign Up</button>
                            </div>
                        </div>
                       
                   
                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
<!--<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>-->
<script src="<?php echo base_url()?>js/clipboard-polyfill.js"></script>
<script>
 
    $( "#sponser_id" ).change(function() {
  var url = '<?php echo base_url(); ?>index.php/login/getsponser_name';

          var data = {
          sponser_id : $( "#sponser_id" ).val(),

          }
          $.post(url, data).done(function(msg){

      $('#sponser_name').val(msg);
        });
});


    document.getElementById('btncopy').addEventListener('click', function() {
        clipboard.copy({
            'text/plain': 'Markup text. Paste me into a rich text editor.',
            'text/html': '<i>here</i> is some <b>rich text</b>'
        }).then(
            function(){console.log('success'); },
            function(err){console.log('failure', err);
        });
    });
    
   

</script>

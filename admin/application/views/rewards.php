<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">Rewards</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">Rewards</li>
                                    </ol>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">

                                          <h4 class="card-title">Rewards</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                  <thead>
                                                      <tr>
                                                          <th>Pairs</th>
                                                          <th>Time</th>
                                                          <th>Status</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                          <th>Pairs</th>
                                                          <th>Time</th>
                                                          <th>Status</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                    
                                                      <tr>
                                                          <td>10</td>
                                                          <td>7 Days</td>
                                                          <td>Not achieved</td>
                                                      </tr>   

                                                      <tr>
                                                          <td>30</td>
                                                          <td>15 Days</td>
                                                          <td>Not achieved</td>
                                                      </tr>   
                                                      <tr>
                                                          <td>100</td>
                                                          <td>30 Days</td>
                                                          <td>Not achieved</td>
                                                      </tr>   
                                                      <tr>
                                                          <td>250</td>
                                                          <td>45 Days</td>
                                                          <td>Not achieved</td>
                                                      </tr>   
                                                      <tr>
                                                          <td>500</td>
                                                          <td>60 Days</td>
                                                          <td>Not achieved</td>
                                                      </tr>    
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
                                
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Cash Back Summary</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Cash Back Summary</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-actions">
                  <?php $total=0; foreach ($spillincome as $value) {
                    $total+=$value['amount']; } ?>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="text-danger"  style="font-weight:600;" >  Total Cash Back Income  :
                        <span style="font-size:20px;"> &#8377;<?php echo $total; ?></span>
                      </span>
                    </div>
                  </div>
                </div>
                <h4 class="card-title">List Cash Back Income</h4>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Direct Match</th>
                        <th>Income</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Direct PV Match</th>
                        <th>Income</th>
                        <th>Date</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php $i=1; foreach ($spillincome as $value) {?>
                      <tr>
                        <td><?php echo  $i; ?></td>
                        <td><?php echo  $value['direct_match']?></td>
                        <td><?php echo  $value['amount']?></td>
                        <td><?php echo  date('Y-m-d',strtotime($value['bonusdate']))?></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- Row -->
          <!-- ============================================================== -->
          <!-- End PAge Content -->
          <!-- ============================================================== -->

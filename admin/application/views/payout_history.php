<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Payout History</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Payout History</li>
                </ol>
              </div>
            </div>
            <div class="container-fluid">
              <div class="col-sm-12">
                <?php
                $error=$this->session->flashdata('error_login');
                echo (!empty($error))?
                "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                <div class="card">
                  <div class="card-header">
                    <h5>List Payout History</h5>
                  </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Direct Income</th>
                            <th>Matching Income</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;
                          $approve_url=base_url()."index.php/home/w_activate";
                          $decline_url=base_url()."index.php/home/w_decline";
                          foreach ($withdraw_list as $value) {?>
                          <tr>
                            <td><?php echo  $i; ?></td>
                           
                            <td><?php echo  date('d-m-Y',strtotime($value['created_on']))?></td>
                            <td><?php echo  $value['amount']?></td>
                            <td><?php echo  $value['direct']?></td>
                            <td><?php echo  $value['username']?></td>
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Direct Income</th>
                            <th>Matching Income</th>
                          </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
    </div>

    <script type='text/javascript'>
      function action(url,id){
        var form = document.createElement("form");
        element1 = document.createElement("input");
        form.action = url;
        form.method = "post";
        element1.name = "id";
        element1.value = id;
        form.appendChild(element1);
        document.body.appendChild(form);
        form.submit();
      }
      function confirmaction(url,id,msg){
        var strconfirm = confirm(msg);
        if (strconfirm == true){
          var form = document.createElement("form");
          element1 = document.createElement("input");
          form.action = url;
          form.method = "post";
          element1.name = "id";
          element1.value = id;
          form.appendChild(element1);
          document.body.appendChild(form);
          form.submit();
        }
      }
    </script>

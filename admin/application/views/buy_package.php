<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Activate Your Membership</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">Buy Package</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Buy Package</h4>
                                <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6>
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/package/activate" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
    <div class="row">

        <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Member Id</label>
                              <!--<select id="member_id" name="member_id" class="form-control">-->

                              <!--    <option value="" selected >Select Member</option>-->
                              <!--    <?php foreach($memberlist as $row){?>-->
                              <!--     <option value="<?php echo $row['member_id']?>"><?php echo $row['username']?></option>-->
                              <!--     <?php }?>-->
                              <!--     </select>-->

                               <input type="text" class="form-control" id="member_id" name="member_id" placeholder=""  value="<?php echo $this->session->userdata('username')?>" >


                            </div>


                                    <div class="form-group">
                                        <label for="exampleInputuname">Member Name</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="member_name" name="member_name" placeholder=""  readonly value="<?php echo $this->session->userdata('name')?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">Current Package</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-user"></i></div>
                                            <input type="text" class="form-control" id="current_package" name="current_package" placeholder="" value="<?php echo $package_price;?>" readonly>
                                        </div>
                                    </div>

                            <div class="form-group">
                              <label for="exampleInputuname">Package</label>

                              <select id="package_id" name="package_id" class="form-control">
                                   
                                </select>
                            </div>
                                    <!--<div class="form-group">-->
                                        <!--<label for="exampleInputuname">E-Pin</label>-->

                                        <!--<select name="pin_id" id="pin_id" class="select2 form-control custom-select" style="width: 100%; height:36px;"  required>-->
                                          <!--<option selected="selected" value="">Select E-Pin</option>-->
                                          <?php //foreach ($epin as $key => $value) {
                                          ?>
                                          <!--<option value="<?php echo $value['id']?>"><?php echo $value['pin']?></option>-->
                                        <?php // }?>
                                        <!--</select>-->

                                                        <!--</div>-->

                                   </div>

                                   <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Payment Method</label>
                              <br>
                              <select id="package_id" name="package_method" class="form-control">
                                   <option value="1">Fund Wallet</option>
                                   <option value="0">E-Wallet</option>
                                   <!-- <option value="2">Fund Wallet (50%) + E-Wallet (50%)</option>
                                   <option value="3">Fund Wallet + E-wallet (Remaining)</option> -->
                                </select>
      
                            </div>


                                    <div class="form-group">
                            <label for="exampleInputuname">Cash Wallet Balance</label>
      <input name="wallet_balance" id="wallet_balance" class="form-control" value="<?php echo $wallet[0]['wallet']?>" readonly>
                                                    </div>

                                    <div class="form-group">
      <label for="exampleInputuname">Fund Wallet Balance</label>
      <input name="fundwallet_balance" id="fundwallet_balance" class="form-control" value="<?php echo $fundwallet[0]['fundwallet']?>" readonly>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputuname">Package Type</label>
                              <br>
      
  <input type="radio" name="package_type" checked value="1" style="position: unset;
    left: -9999px;
    opacity: unset;"> Investment<br>

  
<input type="radio" name="package_type" value="2" style="position: unset;
    left: -9999px;
    opacity: unset;"> Special Investment <i style="background: url('https://www.waveeducoins.com/admin/assets/images/new_blink.gif') no-repeat;background-size:100% 100%; width:40px; height:36px;display: inline-block;position: absolute;margin-top: -8px"></i><br>

  
<input type="radio" name="package_type" value="0"   style="position: unset;
    left: -9999px;
    opacity: unset;"> Education<br>
                            </div>



                                   </div>
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

var edu_pkg = '<option value="25">E-LERNING PAKAGE ($25)</option> <option value="1">BASIC EDU PAKAGE ($100)</option><option value="3">ADVANCE EDU PKG  ($200)</option><option value="5">ADV ELITE EDU PKG ($300)</option><option value="7">EXCELLENT EDU PKG ($500)</option><option value="14">BASIC LEARN & EARN PKG($100)</option><option value="15">ADVANCED LEARN & EARN PKG($200)</option>';

var inv_pkg = '<option value="2">BASIC INVST PKG. ($100)</option><option value="4">ADVANCE INVST PKG ($200)</option><option value="6">SMART INVST PKG ($300)</option><option value="8">ELITE INVEST PKG ($500)</option><option value="9">PREMIUN INVST PKG ($1000)</option><option value="10">SUPER INVST PKG ($2000)</option><option value="11">MEGA INVST PKG  ($3000)</option><option value="12">ROYAL INVST PKG ($5000)</option><option value="13">CROWN INVST PKG ($10000)</option>';

var spl_pkg = '<option value="16">BASIC SPECIAL INVST PKG. ($100)</option><option value="17">ADVANCE SPECIAL INVST PKG ($200)</option><option value="18">SMART SPECIAL INVST PKG ($300)</option><option value="19">ELITE SPECIAL INVEST PKG ($500)</option><option value="20">PREMIUN SPECIAL INVST PKG ($1000)</option><option value="21">SUPER SPECIAL INVST PKG ($2000)</option><option value="22">MEGA SPECIAL INVST PKG  ($3000)</option><option value="23">ROYAL SPECIAL INVST PKG ($5000)</option><option value="24">CROWN SPECIAL INVST PKG ($10000)</option>';


$('#package_id').html(inv_pkg);


$('input[name=package_type]').change(function() {


    if ($('input[name=package_type]:checked').val() == "0"){

        $('#package_id').html(edu_pkg);

    }

    else if ($('input[name=package_type]:checked').val() == "2"){

      $('#package_id').html(spl_pkg);

    }

    else {

      $('#package_id').html(inv_pkg);

    }
});

$( "#member_id" ).change(function() {
  var url = '<?php echo base_url(); ?>index.php/package/get_current_package';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
        $('#current_package').val(msg);
        });


        var url = '<?php echo base_url(); ?>index.php/package/get_member_name';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });
});

</script>

<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
          <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Update Coin Price</h3>
            </div>
              <div class="col-md-7 align-self-center">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                      <li class="breadcrumb-item active">Update Coin Price</li>
                  </ol>
              </div>
          </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Coin Price</h4>
                                <!--<h6 class="card-subtitle"></h6>-->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/home/update_coin" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
    <div class="row">

        <div class="col-md-6">
    <div class="form-group">
                                        <label for="exampleInputuname">Current Price</label>
                            

                               <input type="text" class="form-control" id="coin_price" name="coin_price" placeholder=""  value="<?php echo $wave_coin_price;?>" >


                            </div>

                                    

                                   </div>

                                
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
                                    <button type="reser" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

$( "#member_id" ).change(function() {
  var url = '<?php echo base_url(); ?>index.php/package/get_current_package';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
        $('#current_package').val(msg);
        });


        var url = '<?php echo base_url(); ?>index.php/package/get_member_name';

          var data = {
          member_id : $( "#member_id" ).val(),

          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });
});

</script>

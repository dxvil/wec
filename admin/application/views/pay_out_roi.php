<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5">
            <h3 class="text-themecolor">Daily Closing ROI</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Payout ROI</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">PayOut Generate</h4>
                                <h6 class="card-subtitle">Select Pay Out Date </h6>
                                <form class="form pv-t-20" action="<?php echo base_url();?>index.php/payout/payout_roi" method="post">

<?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Date</label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="ti-calendar"></i></div>
                                           <input type="date" name="payoutdate" required class="form-control" placeholder="dd/mm/yyyy" style="width:60%;">
                                        </div>
                                    </div>

                                    <button type="submit" id="btncopy" class="btn btn-success waves-effect waves-light m-r-10" >Run Process</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

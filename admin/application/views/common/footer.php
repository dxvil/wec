
    <!-- Required Js -->
    <script src="<?php echo base_url() ?>assets/js/vendor-all.min.js"></script>
	  <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url() ?>assets/js/menu-setting.min.js"></script>-->
    <script src="<?php echo base_url() ?>assets/js/pcoded.min.js"></script>
    <!-- amchart js -->
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/amcharts.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/gauge.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/serial.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/light.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/pie.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/ammap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/usaLow.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/radar.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/amchart/js/worldLow.js"></script>
    <!-- notification Js -->
    <script src="<?php echo base_url() ?>assets/plugins/notification/js/bootstrap-growl.min.js"></script>

    <script src="<?php echo base_url() ?>assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/pages/tbl-datatable-custom.js"></script>

    <!-- dashboard-custom js -->
    <script src="<?php echo base_url() ?>assets/js/pages/dashboard-custom.js"></script>

    

</body>

</html>

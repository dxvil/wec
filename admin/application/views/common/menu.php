<!-- [ navigation menu ] start -->
  <nav class="pcoded-navbar">
    <div class="navbar-wrapper">
      <div class="navbar-brand header-logo">
        <a href="<?php echo base_url(); ?>" class="b-brand">
          <div class="b-bg">
            <i class="feather icon-trending-up"></i>
          </div>
          <span class="b-title">Wave Edu Coin</span>
        </a>
        <a class="mobile-menu" id="mobile-collapse" href="#!">
          <span></span>
        </a>
      </div>
      <div class="navbar-content scroll-div">
        <?php $userType= $this->session->userdata('usertype'); if($userType==1){?>
        <ul class="nav pcoded-inner-navbar">
         <li class="nav-item pcoded-menu-caption">
           <label>Navigation</label>
         </li>
         <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item pcoded-hasmenu active pcoded-trigger">
           <a href="<?php echo base_url()?>index.php/home" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-home"></i>
             </span>
             <span class="pcoded-mtext">Dashboard</span>
           </a>
         </li>
         <li data-username="Vertical Horizontal Box Layout RTL fixed static Collapse menu color icon dark" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-layout"></i>
             </span>
             <span class="pcoded-mtext">Configuration</span>
           </a>
           <ul class="pcoded-submenu">
             <li class="">
               <a href="<?php echo base_url()?>index.php/profile" class="" target="_blank">General Settings</a>
             </li>
           </ul>
         </li>
          
         <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-box"></i>
             </span>
             <span class="pcoded-mtext">Coins</span>
           </a>
           <ul class="pcoded-submenu">
               <li class="">
               <a href="<?php echo base_url()?>index.php/home/coin_price" class="">Coin Price</a>
             </li>
             <li class="">
               <a href="<?php echo base_url()?>index.php/home/dollar_price" class="">Coin Payout Price</a>
             </li>
             <li class="">
               <a href="<?php echo base_url()?>index.php/home/list_withdraw_admincoin" class="">List Withdrawal Coin</a>
             </li>
              
             <li class="">
               <a href="<?php echo base_url()?>index.php/home/list_pwithdraw_admincoin" class="">List Pending Withdrawal Coin</a>
             </li>
              
              <li class="">
               <a href="<?php echo base_url()?>index.php/home/list_release_wallet" class="">Release Wallet</a>
             </li>
           </ul>
         </li>

         <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-box"></i>
             </span>
             <span class="pcoded-mtext">Super Packages</span>
           </a>
           <ul class="pcoded-submenu">
               <li class="">
               <a href="<?php echo base_url()?>index.php/home/super_package" class="">Super Package</a>
             </li>
             <li class="">
               <a href="<?php echo base_url()?>index.php/home/virtual_power" class="">Virtual Power</a>
             </li>
           </ul>
         </li>

         


          
          
         <li data-username="widget Statistic Data Table User card Chart" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-layers"></i>
             </span>
             <span class="pcoded-mtext">Pay Out Summary</span>
           </a>
           <ul class="pcoded-submenu">
             <li class="">
               <a href="<?php echo base_url()?>index.php/payout" class="">Daily Closing</a>
             </li>
              
             <li class="">
               <a href="<?php echo base_url()?>index.php/home/payout_history" class="">Daily Closing History</a>
             </li>
              
              <!-- <li class="">
               <a href="<?php echo base_url()?>index.php/payout/roi" class="">ROI Payout</a>
              </li>
              <li class="">
               <a href="<?php echo base_url()?>index.php/payout/level" class="">Level Payout</a>
         </li>  -->
             <li class="">
               <a href="<?php echo base_url()?>index.php/payout/matchingincome" class="">Matching Summary</a>
             </li>
             <!-- <li>
               <a href="<?php echo base_url()?>index.php/payout/matchingincome">Matching Income</a>
             </li> -->
             <li>
               <a href="<?php echo base_url()?>index.php/payout/direct_income">Direct Income</a>
             </li>
              
              <!-- <li>
               <a href="<?php echo base_url()?>index.php/payout/roi_income">ROI Income </a>
              </li>
            <li>
               <a href="<?php echo base_url()?>index.php/payout/cashback_income">Cash Back Income </a>
             </li>
             <li>
               <a href="<?php echo base_url()?>index.php/payout/club_income">Club Income</a>
             </li>
             <li>
               <a href="<?php echo base_url()?>index.php/payout/level_income">Level Income</a>
             </li> -->
           </ul>
         </li>
         <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-box"></i>
             </span>
             <span class="pcoded-mtext">Members</span>
           </a>
           <ul class="pcoded-submenu">
             <li>
               <a href="<?php echo base_url()?>index.php/member">List Members</a>
             </li>
           </ul>
         </li>
         <li data-username="advance components Alert gridstack lightbox modal notification pnotify rating rangeslider slider syntax highlighter Tour Tree view Nestable Toolbar" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-gitlab"></i>
             </span>
             <span class="pcoded-mtext">Fund Management</span>
           </a>
           <ul class="pcoded-submenu">
             <li>
               <a href="<?php echo base_url()?>index.php/fund/list_funds">Fund Confirmation</a>
             </li>
              
             <li>
               <a href="<?php echo base_url()?>index.php/fund/add_deductfund">Add/Deduct Fund</a>
             </li>
             <li>
               <a href="<?php echo base_url()?>index.php/home/withdraw_list">Withdraw Payment</a>
             </li>
             <li>
               <a href="<?php echo base_url()?>index.php/home/withdraw_plist">Pending Withdraw</a>
             </li>
             <li>
               <a href="<?php echo base_url()?>index.php/home/withdraw_clist">Success Withdraw</a>
             </li>
           </ul>
         </li>
         <li data-username="extra components Session Timeout Session Idle Timeout Offline" class="nav-item pcoded-hasmenu">
           <a href="#!" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-package"></i>
             </span>
             <span class="pcoded-mtext">Settings</span>
           </a>
           <ul class="pcoded-submenu">
              <li>
               <a href="<?php echo base_url()?>index.php/home/wallet">Feedback</a>
              </li>
             <li>
               <a href="<?php echo base_url()?>index.php/news">Member News</a>
             </li>
              
             <li>
               <a href="<?php echo base_url()?>index.php/news/list_news">List News</a>
             </li>
           </ul>
         </li>
         <li data-username="Animations" class="nav-item">
           <a href="<?php echo base_url()?>index.php/login/logout" class="nav-link">
             <span class="pcoded-micon">
               <i class="feather icon-aperture"></i>
             </span>
             <span class="pcoded-mtext">Logout</span>
           </a>
         </li>
        </ul>
        <?php } else if($userType==2) {?>
        <ul class="nav pcoded-inner-navbar">
          <li class="nav-item pcoded-menu-caption">
            <label>Navigation</label>
          </li>
          <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item pcoded-hasmenu active pcoded-trigger">
            <a href="<?php echo base_url()?>index.php/home" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-home"></i>
              </span>
              <span class="pcoded-mtext">Dashboard</span>
            </a>
          </li>
          <li data-username="Vertical Horizontal Box Layout RTL fixed static Collapse menu color icon dark" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-layout"></i>
              </span>
              <span class="pcoded-mtext">E-Wallet</span>
            </a>
            <ul class="pcoded-submenu">
              <li>
                <a href="<?php echo base_url()?>index.php/home/wallet">E-Wallet</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/wallet_report">E-Wallet History </a>
              </li>
              
               <li>
                <a href="<?php echo base_url()?>index.php/home/fundwallet_report">Fund-Wallet History </a>
              </li>
              
              <li>
                <a href="<?php echo base_url()?>index.php/home/withdraw_request">E-Wallet withdraw Request</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/member_withdraw_list">E-Wallet withdraw status </a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/member_withdraw_plist">Pending</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/member_withdraw_clist">Success</a>
              </li>
            </ul>
          </li>


          <li data-username="Vertical Horizontal Box Layout RTL fixed static Collapse menu color icon dark" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-layout"></i>
              </span>
              <span class="pcoded-mtext">Bonus-Wallet</span>
            </a>
            <ul class="pcoded-submenu">
              <li>
                <a href="<?php echo base_url()?>index.php/home/b_wallet">Bonus-Wallet</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/b_wallet_report">Bonus-Wallet History </a>
              </li>
            
              
              <li>
                <a href="<?php echo base_url()?>index.php/home/bonus_withdraw_request">Bonus-Wallet withdraw Request</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/home/b_member_withdraw_list">Bonus-Wallet withdraw status </a>
              </li>
              
            </ul>
          </li>



          <li data-username="Animations" class="nav-item">
            <a href="<?php echo base_url()?>index.php/package" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-aperture"></i>
              </span>
              <span class="pcoded-mtext">Upgrade Package</span>
            </a>
          </li>


          <li data-username="Animations" class="nav-item">
            <a href="<?php echo base_url()?>index.php/home/reawrds" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-aperture"></i>
              </span>
              <span class="pcoded-mtext">Rewards <i style="background: url('https://www.waveeducoins.com/admin/assets/images/new_blink.gif') no-repeat;background-size:100% 100%; width:40px; height:36px;display: inline-block;position: absolute;margin-top: -8px"></i></span>
            </a>
          </li>
          
         
          
          <li data-username="widget Statistic Data Table User card Chart" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-layers"></i>
              </span>
              <span class="pcoded-mtext">Coin</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="">
                <a href="<?php echo base_url()?>index.php/home/coin_withdraw" class="">Withdraw Coin</a>
              </li>
              <li class="">
                <a href="<?php echo base_url()?>index.php/home/list_withdraw_coin" class="">List Withdrawal Coin</a>
              </li>
               <li class="">
                <a href="<?php echo base_url()?>index.php/home/coin_history" class="">Coin History</a>
              </li>
              <li class="">
                <a href="<?php echo base_url()?>index.php/home/rcoin_history" class="">Release Coin History</a>
              </li>
            </ul>
          </li>
          
          
          <!--<li data-username="Animations" class="nav-item">-->
          <!--  <a href="<?php echo base_url()?>index.php/home/welcome_letter" class="nav-link">-->
          <!--    <span class="pcoded-micon">-->
          <!--      <i class="feather icon-aperture"></i>-->
          <!--    </span>-->
          <!--    <span class="pcoded-mtext">Welcome Letter</span>-->
          <!--  </a>-->
          <!--</li>-->
          <li data-username="Animations" class="nav-item">
            <a href="<?php echo base_url()?>index.php/home/referal" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-aperture"></i>
              </span>
              <span class="pcoded-mtext">My Referal Link</span>
            </a>
          </li>
          <li data-username="widget Statistic Data Table User card Chart" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-layers"></i>
              </span>
              <span class="pcoded-mtext">Genealogy</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="">
                <a href="<?php echo base_url()?>index.php/team/directmember" class="">My Direct Members</a>
              </li>
              <li class="">
                <a href="<?php echo base_url()?>index.php/team/downline" class="">My Downline</a>
              </li>
              <li class="">
                <a href="<?php echo base_url()?>index.php/team/treeview" class="">My Network Tree</a>
              </li>
             <!--  <li class="">
                <a href="<?php echo base_url()?>index.php/team/matrix_treeview" class="">Level Tree</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/team/matrix_count">Level Wise Member</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/team/matrix_count">Reward</a>
              </li> -->
            </ul>
          </li>
          <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-box"></i>
              </span>
              <span class="pcoded-mtext">Fund Request</span>
            </a>
            <ul class="pcoded-submenu">
              <li>
                <a href="<?php echo base_url()?>index.php/fund">Fund Request</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/fund/list_fund">Fund Request Status</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/fund/list_fund">Fund Request History</a>
              </li>
              
               <li>
                <a href="<?php echo base_url()?>index.php/fund/fund_transfer">Fund Transfer</a>
              </li>
              
              <li>
                <a href="<?php echo base_url()?>index.php/fund/transfered_fund">Trasfered Fund</a>
              </li>
              
              <li>
                <a href="<?php echo base_url()?>index.php/fund/recieved_fund">Recieved Fund</a>
              </li>
              
            </ul>
          </li>
          <li data-username="advance components Alert gridstack lightbox modal notification pnotify rating rangeslider slider syntax highlighter Tour Tree view Nestable Toolbar" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-gitlab"></i>
              </span>
              <span class="pcoded-mtext">Income Summary</span>
            </a>
            <ul class="pcoded-submenu">
              
              <li>
                <a href="<?php echo base_url()?>index.php/team/matchingincome">Matching Income</a>
              </li>
              <li>
                <a href="<?php echo base_url()?>index.php/team/direct_income">Direct Income</a>
              </li>
              <!--<li>-->
              <!--  <a href="<?php echo base_url()?>index.php/team/roi">ROI Income</a>-->
              <!--</li>-->
              
              <!--<li>-->
              <!--  <a href="<?php echo base_url()?>index.php/team/reward_income">Reward Income</a>-->
              <!--</li>-->
            </ul>
          </li>
          <li data-username="extra components Session Timeout Session Idle Timeout Offline" class="nav-item pcoded-hasmenu">
            <a href="#!" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-package"></i>
              </span>
              <span class="pcoded-mtext">News & Support</span>
            </a>
            <ul class="pcoded-submenu">
              <li>
                <a href="<?php echo base_url()?>index.php/home/list_news">Announcement</a>
              </li>
              <!--<li>-->
              <!--  <a href="<?php echo base_url()?>index.php/gift/used_giftcards">Contact US</a>-->
              <!--</li>-->
            </ul>
          </li>
          <li data-username="Animations" class="nav-item">
            <a href="<?php echo base_url()?>index.php/login/logout" class="nav-link">
              <span class="pcoded-micon">
                <i class="feather icon-aperture"></i>
              </span>
              <span class="pcoded-mtext">Logout</span>
            </a>
          </li>
        </ul>
        <?php } ?>
      </div>
    </div>
  </nav>
  <!-- [ navigation menu ] end -->

  <!-- [ Header ] start -->
  <header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
    <div class="m-header">
      <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
      <a href="<?php echo base_url(); ?>" class="b-brand">
        <div class="b-bg">
          <i class="feather icon-trending-up"></i>
        </div>
        <span class="b-title"><h4 style="color:#fff;">Current Coin Price: $<?php echo number_format($wave_coin_price/70,  5);?></h4></span>
      </a>
      </div>
      <a class="mobile-menu" id="mobile-header" href="#!">
        <i class="feather icon-more-horizontal"></i>
      </a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li>
            <a href="#!" class="full-screen" onclick="javascript:toggleFullScreen()">
              <i class="feather icon-maximize"></i>
            </a>
          </li>
          <li class="nav-item">
            <div class="main-search">
              <div class="input-group">
                <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                <a href="#!" class="input-group-append search-close">
                  <i class="feather icon-x input-group-text"></i>
                </a>
                <span class="input-group-append search-btn btn btn-primary">
                  <i class="feather icon-search input-group-text"></i>
                </span>
              </div>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            
            <li><h4 style="color:#fff;">Current Coin Price: $<?php echo number_format($wave_coin_price/70,  5);?></h4></li>
          <li>
            <div class="dropdown drp-user">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon feather icon-settings"></i>
              </a>
              
              
              <div class="dropdown-menu dropdown-menu-right profile-notification">
                <div class="pro-head">
                  <img src="<?php echo base_url()?>uploads/userimage/<?php echo $this->session->userdata('user_image')?>" class="img-radius" alt="User-Profile-Image">
                  <span><?php echo $this->session->userdata('username')?></span>
                  <span class=""><?php echo $this->session->userdata('email')?></span>
                  <a href="<?php echo base_url()?>index.php/login/logout" class="dud-logout" title="Logout">
                    <i class="feather icon-log-out"></i>
                  </a>
                </div>
                <ul class="pro-body">
                  <li>
                    <a href="<?php echo base_url()?>index.php/profile" class="dropdown-item">
                      <i class="feather icon-user"></i> Profile
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/home/wallet" class="dropdown-item">
                      <i class="feather icon-mail"></i> My Balance
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>index.php/profile" class="dropdown-item">
                      <i class="feather icon-lock"></i> Account Setting
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </header>
    <!-- [ Header ] end -->

    <!-- [ chat user list ] start -->
    <section class="header-user-list">
      <div class="h-list-header">
        <div class="input-group">
          <input type="text" id="search-friends" class="form-control" placeholder="Search Friend . . .">
        </div>
      </div>
      <div class="h-list-body">
        <a href="#!" class="h-close-text"><i class="feather icon-chevrons-right"></i></a>
        <div class="main-friend-cont scroll-div">
          <div class="main-friend-list">
            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image ">
                <div class="live-status">3</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Josephin Doe
                  <small class="d-block text-c-green">Typing . . </small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Lary Doe
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-3.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alice
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alia
                  <small class="d-block text-muted">10 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-4.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Suzen
                  <small class="d-block text-muted">15 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image ">
                <div class="live-status">3</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Josephin Doe
                  <small class="d-block text-c-green">Typing . . </small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Lary Doe
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-3.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alice
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alia
                  <small class="d-block text-muted">10 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-4.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Suzen
                  <small class="d-block text-muted">15 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image ">
                <div class="live-status">3</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Josephin Doe
                  <small class="d-block text-c-green">Typing . . </small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Lary Doe
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-3.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alice
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alia
                  <small class="d-block text-muted">10 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-4.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Suzen
                  <small class="d-block text-muted">15 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image ">
                <div class="live-status">3</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Josephin Doe
                  <small class="d-block text-c-green">Typing . . </small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Lary Doe
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-3.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alice
                  <small class="d-block text-c-green">online</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="4" data-status="offline" data-username="Alia">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-1.jpeg" alt="Generic placeholder image">
                <div class="live-status">1</div>
              </a>
              <div class="media-body">
                <h6 class="chat-header">Alia
                  <small class="d-block text-muted">10 min ago</small>
                </h6>
              </div>
            </div>
            <div class="media userlist-box" data-id="5" data-status="offline" data-username="Suzen">
              <a class="media-left" href="#!">
                <img class="media-object img-radius" src="<?php echo base_url() ?>assets/images/user/avatar-4.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body">
                <h6 class="chat-header">Suzen
                  <small class="d-block text-muted">15 min ago</small>
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- [ chat user list ] end -->

    <!-- [ chat message ] start -->
    <section class="header-chat">
      <div class="h-list-header">
        <h6>Josephin Doe</h6>
        <a href="#!" class="h-back-user-list">
          <i class="feather icon-chevron-left"></i>
        </a>
      </div>
      <div class="h-list-body">
        <div class="main-chat-cont scroll-div">
          <div class="main-friend-chat">
            <div class="media chat-messages">
              <a class="media-left photo-table" href="#!">
                <img class="media-object img-radius img-radius m-t-5" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body chat-menu-content">
                <div class="">
                  <p class="chat-cont">hello Datta! Will you tell me something</p>
                  <p class="chat-cont">about yourself?</p>
                </div>
                <p class="chat-time">8:20 a.m.</p>
              </div>
            </div>
            <div class="media chat-messages">
              <div class="media-body chat-menu-reply">
                <div class="">
                  <p class="chat-cont">Ohh! very nice</p>
                </div>
                <p class="chat-time">8:22 a.m.</p>
              </div>
            </div>
            <div class="media chat-messages">
              <a class="media-left photo-table" href="#!">
                <img class="media-object img-radius img-radius m-t-5" src="<?php echo base_url() ?>assets/images/user/avatar-2.jpeg" alt="Generic placeholder image">
              </a>
              <div class="media-body chat-menu-content">
                <div class="">
                  <p class="chat-cont">can you help me?</p>
                </div>
                <p class="chat-time">8:20 a.m.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="h-list-footer">
        <div class="input-group">
          <input type="file" class="chat-attach" style="display:none">
          <a href="#!" class="input-group-prepend btn btn-success btn-attach">
            <i class="feather icon-paperclip"></i>
          </a>
          <input type="text" name="h-chat-text" class="form-control h-send-chat" placeholder="Write hear . . ">
          <button type="submit" class="input-group-append btn-send btn btn-primary">
            <i class="feather icon-message-circle"></i>
          </button>
        </div>
      </div>
    </section>
    <!-- [ chat message ] end -->

<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper" style="overflow-x: scroll!important;">
          <div class="container pt-30">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
              <div class="col-md-4 align-self-center">
                <h3 class="text-themecolor">Welcome letter</h3>
              </div>
              <div class="col-md-4 align-self-center" style="margin-top:15px;">
                <h4 class="text-themecolor" style="display:inline-block;">Reference No  :</h4>
                <p class="text-themecolor" style="display:inline-block; font-size:16px;">500<?php echo $this->session->userdata('member_id');?></p>
              </div>
              <div class="col-md-4 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item" ><a class="text-themecolor"   href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Welcome Letter</li>
                </ol>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="card">

<div class="card-header">
                                            <h5>Welcome Letter</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                <div class="card-body">
                  <table width="90%" style="margin-left:5%">
                    <tbody>
                      <tr>
                        <td colspan="3" align="center" style="height:60px;vertical-align:middle;border-bottom:1px solid #000;padding-top:10px">
                          <table width="90%">
                            <tbody>
                              <tr>
                                <td>
                                  <b> Ever Green Wallet</b>
                                </td>
                                <td style="width:70%;"></td>
                                <td>
                                  <a href="<?php echo base_url()?>index.php/home" >(X)</a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" style="vertical-align:top">
                          <table width="80%" style="margin-left:3%" cellpadding="1"   cellspacing="3">
                            <tbody>
                              <tr>
                                <td style="height:20px" colspan="2">
                                  <br>
                                  Dear Member&nbsp; <span id="ctl00_ContentPlaceHolder1_lblName" style="font-weight:600"><?php echo $this->session->userdata('name');?>,</span>
                                </td>
                              </tr>
                              <tr>
                                <td width="30%">User Id</td>
                                <td>
                                  <span id="ctl00_ContentPlaceHolder1_lblUserid" style="font-weight:600"><?php echo $this->session->userdata('username');?></span>
                                </td>
                              </tr>
                              <tr>
                                <td>City</td>
                                <td>
                                  <span id="ctl00_ContentPlaceHolder1_City" style="font-weight:600"><?php echo $this->session->userdata('username');?></span>
                                </td>
                              </tr>
                              <tr>
                                <td>Mobile Number</td>
                                <td>
                                  <span id="ctl00_ContentPlaceHolder1_lblMob" style="font-weight:600"><?php echo $list_member[0]['contact_number']?></span>
                                </td>
                              </tr>
                              <tr>
                                <td>Package</td>
                                <td>
                                  <span id="ctl00_ContentPlaceHolder1_lblJoin" style="font-weight:600">$<?php echo $package_price;
                                 
                                  ?></span>
                                </td>
                              </tr>
                              <tr>
                                <td>Joining Date</td>
                                <td>
                                  <span id="ctl00_ContentPlaceHolder1_lblJoin" style="font-weight:600"><?php echo date('d-m-Y H:i:s',strtotime($list_member[0]['created_on']))?></span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                        <td class="style4"></td>
                        <td style="vertical-align:top">
                          <table class="style2">
                            <tbody>
                              <tr>
                                <td class="style5" align="left" colspan="2">
                                  <div><br>
                                    <table cellspacing="0" rules="all" border="1"   id="ctl00_ContentPlaceHolder1_gvuploadPhoto" style="border-collapse:collapse;">
                                      <tbody>
                                        <tr>
                                          <td>

                                            <img src="<?php echo base_url();?>uploads/userimage/<?php echo $this->session->userdata('user_image');?>" height="100" border="0" alt="">
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                          <div style="width:95%;margin-left:3%">
                            <br>
                            <br>
                            Thank you For Choosing    <b>Wave Edu Coin</b> For your business type needs, We are confident that you will be very satisfied with the services that we offer.
                            <br>
                            <br>
                            <br>
                            We are delighted you have joined us! Your contribution is important to ensure
                            our sustained success and growth. We hope that your career  here will be a
                            gratifying one.
                            <br>
                            <br>
                            You would get maximum support from the whole of our team and we look forward to
                            having the best relations with you.<br>
                            <br>
                            I welcome to You on the behalf of Wave Edu Coin  ("The Power of Empower").
                            I hope you will find Ever Green Wallet as a cool place to work with !!!<br>
                            <br>
                            Please let me know in case of any problem.<br>
                            You can share your quries with us by sending mail to waveeducoin@gmail.com<br>
                            <br>
                            <br>
                            Thanks and Regards,<br>
                            www.waveeducoin.com
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" style="height:60px"></td>
                      </tr>
                    </tbody>
                  </table>
                  <br>
                </div>
              </div>
            </div>
          </div>

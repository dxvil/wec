<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Change Password</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Change Password</li>
            </ol>
          </div>
          <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <form class="form p-t-20" action="<?php echo base_url()?>index.php/member/update_password" method="post">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputuname">Member ID</label>
                        <input type="text" class="form-control" readonly id="txtreferallink" placeholder="Member ID" value="<?php echo $memberdetails[0]['username'];?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputuname">Member Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Member Name" value="<?php echo $memberdetails[0]['name'];?>">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputuname">New Password</label>
                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter New Password" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputuname">Confirm Password</label>
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required >
                      </div>
                    </div>
                  </div>
                  <button type="submit" id="btncopy" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                  <button type="reset" id="btncopy" class="btn btn-inverse" >Reset</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

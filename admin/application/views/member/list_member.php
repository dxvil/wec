<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">List Member</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">List Member</li>
            </ol>
          </div>
          <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <?php
          $error=$this->session->flashdata('success');
          echo (!empty($error))?
          "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">List Member</h4>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Member Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Member Status</th>
                        <th>Package Price</th>
                        <th>Recharge Username</th>
                        <th>Payment Mode</th>
                        <th>Activation Date</th>
                        <th>Sponser</th>
                        <th>Position</th>
                        <th>Joining Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Member ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Member Status</th>
                        <th>Package Price</th>
                        <th>Recharge Username</th>
                        <th>Payment Mode</th>
                        <th>Activation Date</th>
                        <th>Sponser</th>
                        <th>Position</th>
                        <th>Joining Date</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      <?php
                      $activate=base_url().'index.php/member/activate';
                      $greenmember=base_url().'index.php/member/greenmember';
                      $deactivate=base_url().'index.php/member/deactivate';
                      $edit_url=base_url().'index.php/member/edit_member';
                      $view_url=base_url().'index.php/member/view_member';
                      $password_url=base_url().'index.php/member/change_password';
                      $wallet_reporturl=base_url().'index.php/home/wallet_report_admin';
                      $i=1; foreach ($listmember as $value) {?>
                      <tr>
                        <td><?php echo  $i; ?></td>
                        <td><?php echo  $value['username']?></td>
                        <td><?php echo  $value['name']?></td>
                         <td><?php echo  $value['email']?></td>
                        <td><?php echo  $value['member_status']==0?"<p style='color:red'>Inactive</p>":"<p style='color:green'>Active</p>"?></td>
                        <td>$<?php echo  $value['package_price']?>

                        ( <?php if($value['package_id']==1){ echo 'EDU'; }
                              elseif($value['package_id']==2){ echo 'INVST'; } 
                              elseif($value['package_id']==3){ echo 'EDU'; } 
                              elseif($value['package_id']==4){ echo 'INVST'; } 
                              elseif($value['package_id']==5){ echo 'EDU'; } 
                              elseif($value['package_id']==6){ echo 'INVST'; } 
                              elseif($value['package_id']==7){ echo 'EDU'; }
                              elseif($value['package_id']==8){ echo 'INVEST'; }
                              elseif($value['package_id']==9){ echo 'INVST'; }
                              elseif($value['package_id']==10){ echo 'INVST'; }
                              elseif($value['package_id']==11){ echo 'INVST'; }
                              elseif($value['package_id']==12){ echo 'INVST'; }
                              elseif($value['package_id']==13){ echo 'INVST'; }
                              elseif($value['package_id'] >= 16 && $value['package_id'] <= 24){ echo 'SPECIAL'; }?> ) 
                      
                          </td>
                        <td><?php echo  $value['recharge_id'] == NULL ? '---' :  $value['recharge_id'] ?></td>
                        <?php
                              $Date1 = strtotime(date('Y-m-d', $value['package_add_date']) ).' ';
                              $Date2 = strtotime(date('Y-m-d', strtotime('2020-02-18') ));
                              
                              if ($Date1 < $Date2){
                                  $tx = $value['payment_type'] == 0 ? 'Fund Wallet' : 'Main Wallet';
                              }
                              else{
                                $tx = 'Main Wallet';
                                switch($value['payment_type']){
                                  case 0:
                                  $tx = 'Fund Wallet';
                                  break;
                                  case 1:
                                  $tx = 'Fund Wallet';
                                  break;
                                  case 2:
                                  $tx = '50%-50%';
                                  break;
                                  case 3:
                                  $tx = 'E+R';
                                  break;
                                }
                            }

                        ?>
                        <td><?php echo  $value['member_status']==1 && $value['recharge_id'] != NULL ? $tx : '---' ?></td>
                        <td><?php echo  date('Y-m-d',strtotime($value['package_add_date']))=="1970-01-01"?"--":date('Y-m-d',strtotime($value['package_add_date']))?></td>
                        <td><?php echo  $value['Sponserusername']?></td>
                        <td><?php echo  $value['leg']==1?'Left':'Right';?></td>
                        <td><?php echo  date('d-m-Y',strtotime($value['created_on']))=="1970-01-01"?"--":date('d-m-Y',strtotime($value['created_on']))?></td>
                        <td>
                          <?php if($value['loginstatus']==1){?>
                            <button type="button"
                            onclick="confirmaction('<?php echo $deactivate;?>',<?php echo $value['id']; ?>,'Are you sure you want to Unblock this Member?')" title="UnBlock Member" class="btn btn-success btn-circle"><i class="fa fa-check"></i> </button>
                          <?php } else if($value['loginstatus']==0){ ?>
                            <button title="Block Member" type="button"
                            onclick="action('<?php echo $activate;?>',<?php echo $value['id']; ?>)"   class="btn btn-danger btn-circle"  ><i class="fa fa-times"></i>  </button>
                          <?php } ?>
                          <?php if($value['member_status']==0){ ?>
                            <!--<button title="Green Member" type="button"-->
                            <!--onclick="action('<?php echo $greenmember;?>',<?php echo $value['id']; ?>)" class="btn btn-success btn-circle"><i class="fa fa-check"></i> </button>-->
                          <?php } ?>
                          <button  title="Edit Member Profile"  type="button"
                          onclick="action('<?php echo $edit_url;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </button>
                          <button  title="View Member Profile" type="button"
                          onclick="action('<?php echo $view_url;?>',<?php echo $value['id']; ?>)" class="btn btn-warning btn-circle"><i class="fa fa-list"></i> </button>
                          <button  title="Change Password" type="button"
                          onclick="action('<?php echo $password_url;?>',<?php echo $value['id']; ?>)" class="btn btn-danger btn-circle"><i class="fa fa-briefcase"></i> </button>
                          <button  title="Wallet Report" type="button"
                          onclick="action('<?php echo $wallet_reporturl;?>',<?php echo $value['id']; ?>)" class="btn btn-success btn-circle"><i class="fa fa-file"></i> </button>
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <script type='text/javascript'>
              function action(url,id){
                var form = document.createElement("form");
                element1 = document.createElement("input");
                form.action = url;
                form.method = "post";
                element1.name = "id";
                element1.value = id;
                form.appendChild(element1);
                document.body.appendChild(form);
                form.submit();
              }
              function confirmaction(url,id,msg){
                var strconfirm = confirm(msg);
                if (strconfirm == true){
                  var form = document.createElement("form");
                  element1 = document.createElement("input");
                  form.action = url;
                  form.method = "post";
                  element1.name = "id";
                  element1.value = id;
                  form.appendChild(element1);
                  document.body.appendChild(form);
                  form.submit();
                }
              }
            </script>

<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Received Fund</h3>
          </div>
          <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Received Fund</li>
            </ol>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
          <!-- ============================================================== -->
          <!-- Start Page Content -->
          <!-- ============================================================== -->
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="card-actions">
                  <?php $total=0; foreach ($spillincome as $value) {
                  $total+=$value['amount']; } ?>
                  <div class="row" style="float:right;">
                    <div class="col-md-12">
                      <span class="text-danger"  style="font-weight:600;" >  Total Received Fund  :
                        <span style="font-size:20px;"> &#8377;<?php echo $total; ?></span>
                      </span>
                    </div>
                  </div>
                </div>
                <h4 class="card-title">List Received Fund</h4>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                  <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Amount</th>
                        <th>Received From</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Payment Type</th>
                        <th>Transfered Date</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                       <th>#</th>
                        <th>Amount</th>
                        <th>Received From</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Payment Type</th>
                        <th>Transfered Date</th>
                      </tr>
                    </tfoot>
                  <tbody>
                    <?php $i=1; foreach($spillincome as $value) {?>
                    <tr>
                    <td><?php echo  $i; ?></td>
                    <td><?php echo  $value['amount']?></td>
                    <td>
                      <?php echo  $value['username']?>
                    </td>
                    <td>
                      <?php echo  $value['name']?>
                    </td>
                    <td><?php echo  $value['email']?></td>
                    <td><?php echo  $value['payment_type']=="e_wallet"?"E Wallet":"Fund Wallet";?></td>
                    <td><?php echo  $value['bonusdate'];?></td>
                  </tr>
                  <?php $i++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Row -->
      <!-- Row -->
      <!-- Row -->
      <!-- Row -->
      <!-- Row -->
      <!-- ============================================================== -->
      <!-- End PAge Content -->
      <!-- ============================================================== -->

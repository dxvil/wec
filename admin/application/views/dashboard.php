<!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
        <marquee style="margin-bottom: -25px;margin-top: 30px;font-size: 22px !important;"><p style="color: #f44236;font-size: 18px;">New package topup system are available now. Kindly topup your id carefully ! T&C apply</p></marquee>
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->

                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <!-- [ daily sales section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                          <a href="<?php echo base_url()?>index.php/member/">
                                            <h6 class="mb-4">Total Member:</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-arrow-up text-c-green f-30 m-r-10"></i><?php echo $totalmember[0]['totalmember'];?></h3>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ daily sales section ] end -->

                                <!-- [ Monthly  sales section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                          <a href="<?php echo base_url()?>index.php/team/directmember">
                                            <h6 class="mb-4">Total Active Members:</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center  m-b-0"><i class="feather icon-arrow-down text-c-red f-30 m-r-10"></i><?php echo $atotalmember[0]['atotalmember'];?></h3>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme2" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ Monthly  sales section ] end -->

                                 <div class="col-md-12 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                            <h6 class="mb-4">Total Inactive Member</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-12">
                                                    <h5 class="f-w-300 d-flex align-items-center  m-b-0"><i class="feather icon-arrow-up text-c-green f-30 m-r-10"></i><?php echo $itotalmember[0]['itotalmember'];?></h5>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- [ year  sales section ] start -->
                                
                                
                                <!-- [ daily sales section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                          <a href="<?php echo base_url()?>index.php/member/">
                                            <h6 class="mb-4">Total Coins:</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-arrow-up text-c-green f-30 m-r-10"></i><?php echo round($coins[0]['coins'],0)?></h3>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ daily sales section ] end -->

                                <!-- [ Monthly  sales section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                          <a href="<?php echo base_url()?>index.php/team/directmember">
                                            <h6 class="mb-4">Total Coin Released:</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center  m-b-0"><i class="feather icon-arrow-down text-c-red f-30 m-r-10"></i><?php echo round($r_coins[0]['r_coins'],0);?></h3>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme2" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                          </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ Monthly  sales section ] end -->

                                 <div class="col-md-12 col-xl-4">
                                    <div class="card">
                                        <div class="card-block">
                                            <h6 class="mb-4">Total Inactive Member</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-12">
                                                    <h5 class="f-w-300 d-flex align-items-center  m-b-0"><i class="feather icon-arrow-up text-c-green f-30 m-r-10"></i><?php echo $itotalmember[0]['itotalmember'];?></h5>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- [ year  sales section ] start -->

 <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-auto theme-bg text-white p-t-50 p-b-50">
                                                <i class="feather icon-package f-30"></i>
                                            </div>
                                            <div class="col text-center">
                                                <span class="text-uppercase d-block m-b-10">Total Wallet</span>
                                                <h3 class="f-w-300">$<?php echo   empty($totalbonus[0]['totalbonus'])?0.00:round($totalbonus[0]['totalbonus'],2);?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-auto theme-bg2 text-white p-t-50 p-b-50">
                                                <i class="feather icon-clipboard f-30"></i>
                                            </div>
                                            <div class="col text-center">
                                                <span class="text-uppercase d-block m-b-10">Fund Wallet</span>
                                                <h3 class="f-w-300">$<?php echo   empty($totalfundwallet[0]['totalfundwallet'])?0.00:round($totalfundwallet[0]['totalfundwallet'],2);?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
 <div class="col-md-6 col-xl-4">
                                    <div class="card table-card">
                                        <div class="row-table">
                                            <div class="col-auto theme-bg text-white p-t-50 p-b-50">
                                                <i class="feather icon-briefcase f-30"></i>
                                            </div>
                                            <div class="col text-center">
                                                <span class="text-uppercase d-block m-b-10">Total Earning</span>
                                                <h3 class="f-w-300">$<?php echo   round(($sponser[0]['sponser']+$club[0]['club']+$level[0]['level']+$mbv[0]['mbv']+$cashback[0]['cashback']),2);?></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                             
                               


                              
                              

                                <!-- [ worldLow section ] start -->
                                <div class="col-xl-8 col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Monthly Earnings</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                           <div id="top_x_div" style=" height: 405px;"></div>
                                        </div>
                                    </div>

                                     <!-- [ Recent Users ] start -->
                                <div class="col-xl-12 col-md-12">
                                    <div class="card Recent-Users">
                                        <div class="card-header">
                                            <h5>Recent Joinees</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block px-0 py-3">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <?php foreach($recentmem as $mem){ $rand=(rand(1,3));?>
                        <tr class="unread">
                          <th scope="row">#142<?php echo $rand;?></th>
                          <td>
                            <div>
                              <img class="rounded-circle" style="width:40px;" src="<?php echo base_url() ?>assets/images/user/avatar-<?php echo $rand;?>.jpeg" alt="activity-user"> <?php echo $mem['name']?>
                            </div>
                          </td>
                          <td><?php echo date("d/m/Y",strtotime($mem['created_on']))?></td>
                          <td><?php echo $mem['pin_code']?></td>
                          <td>
                            <?php if($mem['member_status']==0){?>
                            <a href="#!" class="label theme-bg2 text-white f-12">Inactive Member</a>
                        <?php } else { ?>
                            <a href="#!" class="label theme-bg text-white f-12">Active Member</a>
                        <?php } ?>
                          </td>
                          
                        </tr>
                        <?php } ?>
                                                       <!--  <tr >
                                                            <td><img ></td>
                                                            <td>
                                                                <h6 class="mb-1">Isabella Christensen</h6>
                                                                <p class="m-0">Lorem Ipsum is simply dummy</p>
                                                            </td>
                                                            <td>
                                                                <h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>11 MAY 12:56</h6>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                         -->
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ Recent Users ] end -->
                                </div>
                                <!-- [ worldLow section ] end -->

                                <!-- [ statistics year chart ] start -->
                                <div class="col-xl-4 col-md-6">

                                    <div class="card">
                                        <div class="card-block border-bottom">
                                            <div class="row d-flex align-items-center">
                                                <div class="col-auto">
                                                    <i class="feather icon-users f-30 text-c-green"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">$<?php echo   empty($mbv[0]['mbv'])?0.00:round($mbv[0]['mbv'],2);?></h3>
                                                    <span class="d-block text-uppercase">Matching income</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block border-bottom">
                                            <div class="row d-flex align-items-center">
                                                <div class="col-auto">
                                                    <i class="feather icon-user-plus f-30 text-c-blue"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">$<?php echo   empty($direct[0]['direct'])?0.00:round($direct[0]['direct'],2);?></h3>
                                                    <span class="d-block text-uppercase">Direct Income</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block border-bottom">
                                            <div class="row d-flex align-items-center">
                                                <div class="col-auto">
                                                    <i class="feather icon-user-plus f-30 text-c-blue"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">$<?php echo   empty($roi[0]['roi'])?0.00:round($roi[0]['roi'],2);?></h3>
                                                    <span class="d-block text-uppercase">ROI Income</span>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="card-block">
                                            <div class="row d-flex align-items-center">
                                                <div class="col-auto">
                                                    <i class="feather icon-user-plus f-30 text-c-blue"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">Not Achieved</h3>
                                                    <span class="d-block text-uppercase">Reward Achivers</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card bg-c-blue">
                                        <div class="card-header borderless">
                                            <h5 class="text-white">Earnings</h5>
                                        </div>
                                        <div class="card-block" style="padding:13px 25px;">
                                            <div class="earning-text mb-0">
                                                <h3 class="mb-2 text-white f-w-300">$<?php echo $monthly[0]['monthly'];?> <i class="feather icon-arrow-up teal accent-3"></i></h3>
                                                <span class="text-uppercase text-white d-block">Total Earnings of this month</span>
                                            </div>
                                           <!--  <div id="Widget-line-chart" class="WidgetlineChart2 ChartShadow" style="height:180px;"></div> -->
                                        </div>
                                         <div class="card-block bg-c-red" style="padding:10 25px;" >
                                            <div class="earning-text mb-0">
                                                <h3 class="mb-2 text-white f-w-300">$<?php echo $pmonthly[0]['pmonthly'];?> <i class="feather icon-arrow-up teal accent-3"></i></h3>
                                                <span class="text-uppercase text-white d-block">Total Earnings of Previous month</span>
                                            </div>
                                           <!--  <div id="Widget-line-chart" class="WidgetlineChart2 ChartShadow" style="height:180px;"></div> -->
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- [ statistics year chart ] end -->

                                <!-- [social-media section] start -->
                               <!--  <div class="col-md-12 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-facebook-f text-primary f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>12,281</h3>
                                                    <h5 class="text-c-green mb-0">+7.2% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>35,098</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width:60%;height:6px;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>3,539</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme2" role="progressbar" style="width:45%;height:6px;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-twitter text-c-blue f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>11,200</h3>
                                                    <h5 class="text-c-purple mb-0">+6.2% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>34,185</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-green" role="progressbar" style="width:40%;height:6px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>4,567</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-blue" role="progressbar" style="width:70%;height:6px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-google-plus-g text-c-red f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>10,500</h3>
                                                    <h5 class="text-c-blue mb-0">+5.9% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>25,998</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width:80%;height:6px;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>7,753</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme2" role="progressbar" style="width:50%;height:6px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- [social-media section] end -->

                                <!-- [ rating list ] starts-->
                               <!--  <div class="col-xl-4 col-md-6">
                                    <div class="card user-list">
                                        <div class="card-header">
                                            <h5>Rating</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center m-b-20">
                                                <div class="col-6">
                                                    <h2 class="f-w-300 d-flex align-items-center float-left m-0">4.7 <i class="fas fa-star f-10 m-l-10 text-c-yellow"></i></h2>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="d-flex  align-items-center float-right m-0">0.4 <i class="fas fa-caret-up text-c-green f-22 m-l-10"></i></h6>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <h6 class="align-items-center float-left"><i class="fas fa-star f-10 m-r-10 text-c-yellow"></i>5</h6>
                                                    <h6 class="align-items-center float-right">384</h6>
                                                    <div class="progress m-t-30 m-b-20" style="height: 6px;">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <h6 class="align-items-center float-left"><i class="fas fa-star f-10 m-r-10 text-c-yellow"></i>4</h6>
                                                    <h6 class="align-items-center float-right">145</h6>
                                                    <div class="progress m-t-30  m-b-20" style="height: 6px;">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <h6 class="align-items-center float-left"><i class="fas fa-star f-10 m-r-10 text-c-yellow"></i>3</h6>
                                                    <h6 class="align-items-center float-right">24</h6>
                                                    <div class="progress m-t-30  m-b-20" style="height: 6px;">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <h6 class="align-items-center float-left"><i class="fas fa-star f-10 m-r-10 text-c-yellow"></i>2</h6>
                                                    <h6 class="align-items-center float-right">1</h6>
                                                    <div class="progress m-t-30  m-b-20" style="height: 6px;">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <h6 class="align-items-center float-left"><i class="fas fa-star f-10 m-r-10 text-c-yellow"></i>1</h6>
                                                    <h6 class="align-items-center float-right">0</h6>
                                                    <div class="progress m-t-30  m-b-20" style="height: 6px;">
                                                        <div class="progress-bar" role="progressbar" style="width:0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- [ rating list ] end -->


                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ Main Content ] end -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  (function($){
      //start graph code
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Opening Move', 'Months'],
          <?php foreach ($graphrecord as $key => $val): ?>
            ['<?php echo $val["month"] ?>', <?php echo $val["total"] ?>],
          <?php endforeach; ?>
        ]);

        var options = {
          title: '',
          width: 550,
          legend: { position: 'none' },
          chart: { title: '',
                   subtitle: '' },
          bars: 'vertical', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'Months'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };
      //end of graph code

  })(jQuery)
</script>

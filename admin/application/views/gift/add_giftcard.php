<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Buy Gift Card</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Buy Gift Card</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Buy Gift Card</h4> -->
                                <!-- <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/gift/admin_buy" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
        <label for="exampleInputuname">Select Gift Card</label>

            <select name="package" id="package" class="form-control" required onchange="getpackage()">
              <option selected="selected" value="">Select Package</option>
              <option value="1">Intas Bronze(1000/-)</option>
               <option value="2">Intas Silver(2000/-)</option>
               <option value="3">Intas Gold(5000/-)</option>
               <option value="4">Intas Platinum(8000/-)</option>
               <option value="5">Intas Diamond(12000/-)</option>
               <option value="6">Intas Kohinoor(15000/-)</option>
               <option value="7">Intas High 10PV(9999/-)</option>
               <option value="8">Intas High 20PV(19999/-)</option>
              
            </select>

        </div>
                                    </div>

                                    <div class="col-md-4">
                              <div class="form-group">
                              <label for="exampleInputuname">Gift Value</label>

                                  <input name="gift_value" id="gift_value" class="form-control" value="0.00" readonly>


                          </div>
                          </div>

                          <div class="col-md-4">
                    <div class="form-group">
                    <label for="exampleInputuname">
No. of EPIN</label>

                        <input name="pin" id="pin" class="form-control" value=""  placeholder="Enter no of PIN" required>


                </div>
                </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputuname">
                                    Transfer To Member ID</label>

                                    <select name="member_id" id="member_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" onchange="getmembername()" required>
                                    <option selected="selected" value="">Select Member</option>
                                    <?php foreach ($members as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value['member_id']?>"><?php echo $value['username']?></option>
                                    <?php  }?>
                                    </select>

                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputuname">Transfer To Member Name</label>

                                    <input name="member_name" id="member_name" class="form-control" value=""  placeholder="Transfer To Member Name"  readonly required>

                                    </div>
                                    </div>

                                            </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<script>
        function getpackage()
        {
          var package=  $('#package').val();

          var url = '<?php echo base_url(); ?>index.php/gift/getpackage';
          var data = {
          package : package,

          }
          $.post(url, data).done(function(msg){
          $('#gift_value').val(msg);
        });

        }

        function getmembername()
        {
          var member_id=  $('#member_id').val();
          var url = '<?php echo base_url(); ?>index.php/gift/getmembername';
          var data = {
          member_id : member_id,
          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });

        }

        function getwallet(wallet,id)
        {
          // var wallet=  $('#wallet').val();
          var isChecked = $('#'+id+':checked').val()?true:false;


if(isChecked){
  var url = '<?php echo base_url(); ?>index.php/gift/getwallet';
  var data = {
    wallet : wallet,
  }
  $.post(url, data).done(function(msg){
if(wallet==2)
{
    $('#wallet_balance').val(msg);
}
if(wallet==3)
{
    $('#iwallet_balance').val(msg);
}
});
}
else{
  if(wallet==2)
  {
      $('#wallet_balance').val('');
  }
  if(wallet==3)
  {
      $('#iwallet_balance').val('');
  }
}

        }


        </script>

        <script>
   jQuery(document).ready(function() {

       // For select 2
       $(".select2").select2();
     });
     </script>



<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Send EPIN</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Send EPIN</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Buy Gift Card</h4> -->
                                <!-- <h6 class="card-subtitle">Activate Your Membership by joining with this package</h6> -->
                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/gift/send" method="post">

  <?php
$error=$this->session->flashdata('danger');
echo (!empty($error))?
    "<div class='alert alert-danger'>".$this->session->flashdata('danger')."</div>" : ''  ?>

<?php
$error=$this->session->flashdata('success');
echo (!empty($error))?
    "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
        <label for="exampleInputuname">EPIN Type</label>

            <select name="package" id="package" class="form-control" onchange="getpackage()" required>
              <option selected="selected" value="">Select EPIN</option>
              <option value="1">Joining Package (1500)</option>
             
            </select>
        </div>
                                    </div>

                                    <div class="col-md-4">
                              <div class="form-group">
                              <label for="exampleInputuname">EPIN Available:</label>

                                  <input name="gift_available" id="gift_available" class="form-control" value="0" readonly>

                          </div>
                          </div>

                                  </div>

                                  <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                      <label for="exampleInputuname">No of Gift Card To Transfer</label>
  <input type="number" name="epin" id="epin" class="form-control" value="" placeholder="Enter No">

                                      </div>
                                                                  </div>

                                                                  <div class="col-md-4">
                                                            <div class="form-group">
                                                            <label for="exampleInputuname">
Transfer To Member ID</label>

<select name="member_id" id="member_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" onchange="getmembername()" required>
  <option selected="selected" value="">Select Member</option>
  <?php foreach ($members as $key => $value) {
  ?>
  <option value="<?php echo $value['member_id']?>"><?php echo $value['username']?></option>
<?php  }?>
</select>

                                                        </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                  <div class="form-group">
          <label for="exampleInputuname">Transfer To Member Name</label>

                                              <input name="member_name" id="member_name" class="form-control" value=""  placeholder="Transfer To Member Name"  readonly required>

                                              </div>
                                              </div>
                                              </div>

                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title">List Gift Cards</h4>
                              <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                              <div class="table-responsive m-t-40">
                                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>#</th>
                                              <th>Created Date</th>
                                              <th>Gift Card</th>
                                              <th>Gift Card Value</th>
                                              <th>Used By</th>
                                              <th>Date of Used</th>
                                              <th>Remarks</th>

                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>#</th>
                                            <th>Created Date</th>
                                            <th>Gift Card</th>
                                            <th>Gift Card Value</th>
                                            <th>Used By</th>
                                            <th>Date of Used</th>
                                            <th>Remarks</th>

                                          </tr>
                                      </tfoot>
                                      <tbody>
                                            <?php $i=1; foreach ($giftcards as $value) {?>
                                          <tr>
                                             <td><?php echo  $i; ?></td>
                                              <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                                              <td><?php echo  $value['pin']?></td>
                                              <td>$<?php echo  $value['amount']?></td>
                                              <td><?php echo  $value['used_by']?></td>
                                              <td><?php echo  $value['used_date']?></td>
                                              <td><?php echo  $value['remarks']?></td>
                                          </tr>
            <?php $i++; } ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

<script>
        function getmembername()
        {
          var member_id=  $('#member_id').val();
          var url = '<?php echo base_url(); ?>index.php/gift/getmembername';
          var data = {
          member_id : member_id,
          }
          $.post(url, data).done(function(msg){
          $('#member_name').val(msg);
        });

        }

        function getcards()
        {
          var package=  $('#package').val();

            var url = '<?php echo base_url(); ?>index.php/gift/getcards';
            var data = {
              package : package,
            }
            $.post(url, data).done(function(msg){

          $('#wallet_balance').val(msg);
          });
        }
        function getpackage()
        {
          var package=  $('#package').val();

            var url = '<?php echo base_url(); ?>index.php/gift/count_gift';
            var data = {
              package : package,
            }
            $.post(url, data).done(function(msg){

          $('#gift_available').val(msg);
          });
        }



        </script>

        <script>
   jQuery(document).ready(function() {

       // For select 2
       $(".select2").select2();
     });
     </script>

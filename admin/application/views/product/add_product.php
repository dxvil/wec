<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Add Product</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add Product</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">

                                <form class="form p-t-20" action="<?php echo base_url()?>index.php/product/save_product" method="post" accept-charset="utf-8" enctype="multipart/form-data">

                                  <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputuname">Product ID</label>
                                            <input type="text" class="form-control"  id="product_id" name="product_id" placeholder="Product ID" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputuname">Product Name
</label>
                                            <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" value="" required>



                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuname">MRP
</label>

                                            <input type="number" class="form-control" id="product_mrp" name="product_mrp" placeholder="Product MRP" value="" required>

                                    </div>
                                  </div>


                                  <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="exampleInputuname">DP</label>

                                          <input type="number" class="form-control" id="product_dp"  name="product_dp" placeholder="Product DP" value="">

                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputuname">Tax

                                  </label>

                                  <select name="tax_id" id="tax_id" class="form-control" required>
              <option selected="selected" value="">Select Tax</option>
<?php foreach ($tax as $row){?>
              <option value="<?php echo $row['id']?>"><?php echo $row['tax_name']?></option>
            <?php } ?>
            </select>

                                  </div>

                                  <div class="form-group">
                                      <label for="exampleInputuname">BV

                                  </label>
                                          <input type="number" class="form-control" id="product_bv"  name="product_bv" placeholder="Product BV" value="">
                                  </div>
                                  </div>

                                  <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="exampleInputuname">Opening Quantity</label>
                                          <input type="number" class="form-control" id="quantity"  name="quantity" placeholder="Opening Quantity" value="" required>

                                  </div>
                                  
                                   <div class="form-group">
                                      <label for="exampleInputuname">Image</label>
                                        <input type="file" class="form-control" id="userfile" name="userfile" required>

                                  </div>

                                  </div>
                                  
                                   
                                  </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Add</button>
  <button type="reset"  class="btn btn-inverse" >Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->


        <!-- Row -->

        <!-- Row -->
        <!-- Row -->

        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->

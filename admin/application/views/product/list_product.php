

                        <!-- ============================================================== -->
                        <!-- Page wrapper  -->
                        <!-- ============================================================== -->
                        <div class="page-wrapper">
                            <!-- ============================================================== -->
                            <!-- Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <div class="row page-titles">
                                <div class="col-md-5 align-self-center">
                                    <h3 class="text-themecolor">List Product</h3>
                                </div>
                                <div class="col-md-7 align-self-center">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                        <li class="breadcrumb-item active">List Product</li>
                                    </ol>
                                </div>
                                <div>
                                    <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- End Bread crumb and right sidebar toggle -->
                            <!-- ============================================================== -->
                            <!-- ============================================================== -->
                            <!-- Container fluid  -->
                            <!-- ============================================================== -->
                            <div class="container-fluid">

                                <!-- ============================================================== -->
                                <!-- Start Page Content -->
                                <!-- ============================================================== -->

                                <?php
        $error=$this->session->flashdata('success');
        echo (!empty($error))?
            "<div class='alert alert-success'>".$this->session->flashdata('success')."</div>" : ''  ?>
                                <div class="col-lg-12">
                                  <div class="card">
                                      <div class="card-body">

                                          <h4 class="card-title">List Product</h4>
                                          <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                          <div class="table-responsive m-t-40">
                                              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                      <tr>

                                                          <th>#</th>
                                                          <th>Product Id</th>
                                                          <th>Product Name</th>
                                                          <th>MRP</th>
                                                          <th>Tax</th>
                                                          <th>Bv</th>
                                                          <th>DP</th>
                                                          <th>Quantity</th>
                                                          <th>Date</th>
                                                          <th>Action</th>
                                                      </tr>
                                                  </thead>
                                                  <tfoot>
                                                      <tr>
                                                        <th>#</th>
                                                        <th>Product Id</th>
                                                        <th>Product Name</th>
                                                        <th>MRP</th>
                                                        <th>Tax</th>
                                                        <th>Bv</th>
                                                        <th>DP</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                      </tr>
                                                  </tfoot>
                                                  <tbody>
                                                        <?php
                                                        $activate=base_url().'index.php/product/activate';
                                                        $deactivate=base_url().'index.php/product/deactivate';
                                                        $edit_url=base_url().'index.php/product/edit_product';

                                                         $i=1; foreach ($listmember as $value) {?>
                                                      <tr>
                                                          <td><?php echo  $i; ?></td>
                                                          <td><?php echo  $value['product_id']?></td>
                                                          <td><?php echo  $value['product_name']?></td>
                                                          <td><?php echo  $value['product_mrp']?></td>
                                                          <td><?php echo  $value['tax_name']?></td>
                                                          <td><?php echo  $value['product_bv']?></td>
                                                          <td><?php echo  $value['product_dp']?></td>
                                                          <td><?php echo  $value['quantity']?></td>
                                                          <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                                                          <td>

<?php if($value['status']==0){?>

                                                            <button type="button"
                                                         onclick="confirmaction('<?php echo $deactivate;?>',<?php echo $value['id']; ?>,'Are you sure you want to Delete this Product?')" class="btn btn-danger btn-circle"  title="Delete Product"><i class="fa fa-times"></i> </button>

<?php }
?>
                                                        <button  title="Edit Member Profile"  type="button"
                                                       onclick="action('<?php echo $edit_url;?>',<?php echo $value['id']; ?>)" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i> </button>

                                                      </tr>
<?php $i++; } ?>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                <!-- Row -->


                                <!-- Row -->

                                <!-- Row -->
                                <!-- Row -->

                                <!-- Row -->
                                <!-- ============================================================== -->
                                <!-- End PAge Content -->
                                <!-- ============================================================== -->
                                <script type='text/javascript'>

                                function action(url,id)
                                {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();

                                }

                                function confirmaction(url,id,msg)
                                {
                                 var strconfirm = confirm(msg);
                                 if (strconfirm == true)
                                            {

                                  var form = document.createElement("form");
                                  element1 = document.createElement("input");

                                  form.action = url;
                                  form.method = "post";

                                  element1.name = "id";
                                  element1.value = id;
                                  form.appendChild(element1);
                                  document.body.appendChild(form);
                                  form.submit();
                                 }
                                }
                                </script>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>WAVE EDU COIN - Signin</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="CodedThemes">

    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.ico" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/animation/css/animate.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/layouts/dark.css">

</head>

<body>
    <div class="auth-wrapper aut-bg-img" style="background-image: url('<?php echo base_url() ?>assets/images/bg-images/bg3.jpeg');">
        <div class="auth-content">
            <div class="text-white">
              <form class="" id="mm" method="post" action="<?php echo base_url()?>index.php/login/check_login">
                <?php
                  $error=$this->session->flashdata('error_login');
                  echo (!empty($error))?
                  "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                  
                  
                  <div class='alert alert-danger' id="diverror" style="display:none;">Please Check your phone and enter the OTP</div>

                <div class="card-body text-center">
                    <div class="mb-4">
                        <i class="feather icon-unlock auth-icon"></i>
                    </div>
                    <h3 class="mb-4 text-white">Login</h3>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Username" name="email" id="username">
                    </div>
                    <div class="input-group mb-4">
                        <input type="password" class="form-control" placeholder="password" name="password">
                    </div>
                    <div class="input-group mb-4" id="divotp" <?php echo $this->uri->segment(3)=="admincheck"?'':"style='display:none;'"?>>
                        <input type="text" class="form-control" placeholder="OTP" name="otp" id="otp" value="<?php echo $this->uri->segment(3)=='admincheck'?$this->uri->segment(4):''?>">
                    </div>
                    <div class="form-group text-left">
                        <div class="checkbox checkbox-fill d-inline">
                            <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" checked="">
                            <label for="checkbox-fill-a1" class="cr"> Save credentials</label>
                        </div>
                    </div>
                    <button class="btn btn-primary shadow-2 mb-4" type="submit" id="submit">Login</button>
                    <p class="mb-2 text-muted">Forgot password? <a class="text-white" href="<?php echo base_url() ?>index.php/login/forget_password">Reset</a></p>
                    <p class="mb-0 text-muted">Don’t have an account? <a class="text-white" href="<?php echo base_url()?>index.php/login/signup">Signup</a></p>
                </div>
              </form>
            </div>
        </div>
    </div>

    <!-- Required Js -->
    <script src="../assets/js/vendor-all.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/pcoded.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
 <script type="text/javascript">
      $( "#mm" ).submit(function() {
         
          if(($("#username").val()=="admin" || $("#username").val()=="Admin") && $("#otp").val()==""){
             
             $( "#divotp" ).show();
              $( "#diverror" ).show();
             
              
              var url = '<?php echo base_url(); ?>index.php/login/admin_otp';
          var data = {
            sponser_id : '',
          }
          $.post(url, data).done(function(msg){
              
          });
          
          return false;
          }
      
          else{
                $( "#divotp" ).hide(); 
                $( "#diverror" ).hide();
          }
          
         
        
      });

    
    </script>
</body>
<!-- Copied from http://html.codedthemes.com/datta-able/bootstrap/default/auth-signin-v3.html by Cyotek WebCopy 1.5.0.516, Friday, February 22, 2019, 3:40:33 PM -->
</html>


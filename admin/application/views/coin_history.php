<!-- [ Main Content ] start -->
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <!-- [ breadcrumb ] start -->
            <div class="row page-titles">
              <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Coin History</h3>
              </div>
              <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Coin History</li>
                </ol>
              </div>
            </div>
            <div class="container-fluid">
              <div class="col-sm-12">
                <?php
                $error=$this->session->flashdata('error_login');
                echo (!empty($error))?
                "<div class='alert alert-danger'>".$this->session->flashdata('error_login')."</div>" : ''  ?>
                <div class="card">
                  <div class="card-header">
                    <h5>List Coin History</h5>
                  </div>
                  <div class="card-block">
                    <div class="table-responsive">
                      <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Package Name</th>
                            <th>Package Price</th>
                            <th>Coin Received</th>
                            <th>Released Coin</th>
                            <th>Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;
                          
                          foreach ($withdraw_list as $value) {?>
                          <tr>
                            <td><?php echo  $i; ?></td>
                            <td><?php if($value['package_id']==1){ echo 'BASIC EDU PAKAGE'; }
                                                    elseif($value['package_id']==2 || $value['package_id']==16){ echo 'BASIC INVST PKG'; } 
                                                    elseif($value['package_id']==3){ echo 'ADVANCE EDU PKG'; } 
                                                    elseif($value['package_id']==4 || $value['package_id']==17){ echo 'ADVANCE INVST PKG'; } 
                                                    elseif($value['package_id']==5){ echo 'ADV ELITE EDU PKG'; } 
                                                    elseif($value['package_id']==6 || $value['package_id']==18){ echo 'SMART INVST PKG'; } 
                                                    elseif($value['package_id']==7){ echo 'EXCELLENT EDU PKG'; }
                                                    elseif($value['package_id']==8 || $value['package_id']==19){ echo 'ELITE INVEST PKG'; }
                                                    elseif($value['package_id']==9 || $value['package_id']==20){ echo 'PREMIUN INVST PKG'; }
                                                    elseif($value['package_id']==10 || $value['package_id']==21){ echo 'SUPER INVST PKG'; }
                                                    elseif($value['package_id']==11 || $value['package_id']==22){ echo 'MEGA INVST PKG'; }
                                                    elseif($value['package_id']==12 || $value['package_id']==23){ echo 'ROYAL INVST PKG'; }
                                                    elseif($value['package_id']==13 || $value['package_id']==24){ echo 'CROWN INVST PKG'; }
                                                    ?> </td>
                            <td>$<?php  echo $value['package_price'];?></td>
                            <td><?php echo  round($value['coin'])?></td>
                            <td><?php echo  round($value['r_coin'],0)?></td>
                            <td><?php echo  date('Y-m-d',strtotime($value['created_on']))?></td>
                            
                        </tr>
                        <?php $i++; } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                        <th>#</th>
                            <th>Package Name</th>
                            <th>Package Price</th>
                            <th>Coin Received</th>
                            <th>Released Coin</th>
                            <th>Date</th>
                        </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
    </div>

    <script type='text/javascript'>
      function action(url,id){
        var form = document.createElement("form");
        element1 = document.createElement("input");
        form.action = url;
        form.method = "post";
        element1.name = "id";
        element1.value = id;
        form.appendChild(element1);
        document.body.appendChild(form);
        form.submit();
      }
      function confirmaction(url,id,msg){
        var strconfirm = confirm(msg);
        if (strconfirm == true){
          var form = document.createElement("form");
          element1 = document.createElement("input");
          form.action = url;
          form.method = "post";
          element1.name = "id";
          element1.value = id;
          form.appendChild(element1);
          document.body.appendChild(form);
          form.submit();
        }
      }
    </script>
